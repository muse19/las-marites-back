<?php

namespace Database\Seeders;

use App\Models\Unit;
use Illuminate\Database\Seeder;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = [
              [
                  'nombre' => 'Unidad',
                  'simbolo' => 'ud.'
              ],
              [
                'nombre' => 'Kilogramo',
                'simbolo' => 'kg'
             ],
             [
                'nombre' => 'Litro',
                'simbolo' => 'lt'
             ],
        ];

        foreach ($units as $unit){
            Unit::create($unit);
        }
    }
}
