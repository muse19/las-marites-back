<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'nombre' => 'Recepcionista',
                'slug' => 'receptionist'
            ],
            [
                'nombre' => 'Administrador',
                'slug' => 'admin'
            ],
            [
                'nombre' => 'Cliente',
                'slug' => 'customer'
            ],
            [
                'nombre' => 'Cocinero',
                'slug' => 'kitchener'
            ],
        ];

       Role::insert($roles);
    }
}
