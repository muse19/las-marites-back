<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(FacilitySeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(RoomTypeSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(UnitSeeder::class);
        $this->call(RoomSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(PaymentMethodSeeder::class);
    }
}
