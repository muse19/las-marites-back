<?php

namespace Database\Seeders;

use App\Models\RoomType;
use Illuminate\Database\Seeder;

class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'nombre' => 'Doble',
                'capacidad' => 2
            ],
            [
                'nombre' => 'Triple',
                'capacidad' => 3
            ],
            [
                'nombre' => 'Quíntuple',
                'capacidad' => 5
            ],
        ];

        foreach ($types as $type){
            RoomType::create($type);
        }
    }
}
