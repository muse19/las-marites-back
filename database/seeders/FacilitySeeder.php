<?php

namespace Database\Seeders;

use App\Models\Facility;
use Illuminate\Database\Seeder;

class FacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facilities = [
            [
               'nombre' => 'Tv',
               'icono' => 'mdi-television'
            ],
            [
                'nombre' => 'Aire acondicionado',
                'icono' => 'mdi-air-conditioner'
            ],
            [
                'nombre' => 'Wi-Fi',
                'icono' => 'mdi-wifi'
            ],
            [
                'nombre' => 'Teléfono',
                'icono' => 'mdi-phone-classic'
            ],
        ];

        foreach ($facilities as $facility){
            Facility::create($facility);
        }
    }
}
