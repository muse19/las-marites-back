<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'rol_id' => 2,
            'cedula' => '26082103',
            'nombre' => 'Wendy',
            'apellido' => 'Hurtado',
        ]);

        Account::create([
            'usuario_id' => $user->id,
            'correo' => 'wendy@gmail.com',
            'contrasenia' => bcrypt('12345678')
        ]);

        $user = User::create([
            'rol_id' => 3,
            'cedula' => '26082105',
            'nombre' => 'Jesus',
            'apellido' => 'Calderin',
        ]);

        Account::create([
            'usuario_id' => $user->id,
            'correo' => 'jesus@gmail.com',
            'contrasenia' => bcrypt('12345678')
        ]);
    }
}
