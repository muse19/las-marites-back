<?php

namespace Database\Seeders;

use App\Models\Facility;
use App\Models\Room;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $description = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis eius eveniet provident qui! Ut, voluptate possimus. Beatae enim nihil maxime iste mollitia deserunt!";
        $rooms = [
            [
                'numero' => '101',
                'nombre' => 'Suite',
                'precio' => '100.00',
                'tipo_habitacion_id' => 1,
                'descripcion' => $description,
                'estado' => Room::ENABLE,
            ],
            [
                'numero' => '102',
                'nombre' => 'Suite',
                'precio' => '110.00',
                'tipo_habitacion_id' => 2,
                'descripcion' => $description,
                'estado' => Room::ENABLE,

            ],
            [
                'numero' => '103',
                'nombre' => 'Town House',
                'precio' => '120.00',
                'tipo_habitacion_id' => 3,
                'descripcion' => $description,
                'estado' => Room::ENABLE,
            ],
            [
                'numero' => '104',
                'nombre' => 'Town House',
                'precio' => '130.00',
                'tipo_habitacion_id' => 2,
                'descripcion' => $description,
                'estado' => Room::ENABLE,
            ],
            [
                'numero' => '105',
                'nombre' => 'Town House',
                'precio' => '140.00',
                'tipo_habitacion_id' => 3,
                'descripcion' => $description,
                'estado' => Room::ENABLE,
            ],
            [
                'numero' => '106',
                'nombre' => 'Town House',
                'precio' => '150.00',
                'tipo_habitacion_id' => 2,
                'descripcion' => $description,
                'estado' => Room::ENABLE,
            ],
            [
                'numero' => '107',
                'nombre' => 'Village House',
                'precio' => '160.00',
                'tipo_habitacion_id' => 2,
                'descripcion' => $description,
                'estado' => Room::ENABLE,
            ],
            [
                'numero' => '108',
                'nombre' => 'Suite Deluxe',
                'precio' => '150.00',
                'tipo_habitacion_id' => 1,
                'descripcion' => $description,
                'estado' => Room::ENABLE,
            ],
            [
                'numero' => '109',
                'nombre' => 'Town House',
                'precio' => '180.00',
                'tipo_habitacion_id' => 3,
                'descripcion' => $description,
                'estado' => Room::ENABLE,
            ],
            [
                'numero' => '110',
                'nombre' => 'Suite',
                'precio' => '100.00',
                'tipo_habitacion_id' => 1,
                'descripcion' => $description,
                'estado' => Room::ENABLE,
            ],
        ];

        foreach ($rooms as $room){
            $room = Room::create($room);

            $room->facilities()->sync(Facility::all()->pluck('id')->toArray());

            $roomsNumbers = [];


            while (count($roomsNumbers) < 4) {
                $number = rand(1,10);
                if(in_array($number, $roomsNumbers) === false){
                    $room->pictures()->create([
                        'imagen' => "room-{$number}.jpg",
                    ]);
                    array_push($roomsNumbers, $number);
                }
            }

        }
    }
}
