<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    public function run()
    {
        $currencies = [
            [
                'codigo' => 'VES',
                'nombre' => 'Bolívar Digital',
                'tasa' => '1.00',
            ],
            [
                'codigo' => 'USD',
                'nombre' => 'Dólar Estadounidense',
                'tasa' => '5.07',
            ]

        ];

        foreach($currencies as $currency){
            Currency::create($currency);
        }
    }
}
