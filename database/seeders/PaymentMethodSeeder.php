<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentMethods = [
            'Paypal',
            'Stripe',
            'Transferencia bancaria',
            'Efectivo',
            'Tarjeta de debito',
            'Tarjeta de credito',
           
           
        ];

        foreach($paymentMethods as $paymentMethod){
            PaymentMethod::create([
                'nombre' => $paymentMethod,
            ]);
        }
    }
}
