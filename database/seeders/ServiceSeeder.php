<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            [
                'nombre' => 'Lavandería',
                'precio' => 1500
            ],
            [
                'nombre' => 'Traslado',
                'precio' => 2500
            ],
        ];

        foreach ($services as $service){
            Service::create($service);
        }
    }
}
