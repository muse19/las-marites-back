<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
          [
              'nombre' => 'pendiente',
              'color' => '#ff7d19',
          ],
          [
            'nombre' => 'pago sin verificar',
            'color' => '#ffd40f',
          ],
          [
              'nombre' => 'confirmada',
              'color' => '#22820e',
          ],
          [
              'nombre' => 'anulada',
              'color' => '#d1291d',
          ],
          [
              'nombre' => 'check-in',
              'color' => '#0c53c4',
          ],
          [
              'nombre' => 'check-out',
              'color' => '#686c6e',
          ]
        ];

        foreach ($statuses as $status){
            Status::create($status);
        }
    }
}
