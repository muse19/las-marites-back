<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Contact;
use App\Models\Gallery;
use App\Models\SocialNetwork;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::create([
            'nombre' => 'Posada Las Marites',
            'logo' => 'logo.png',
            'direccion' => 'Calle Las Marítes, 1',
            'sobre_nosotros' => 'Somos una posada de calidad, con una gran variedad de servicios y una gran variedad de opciones para que disfrutes de una estancia inolvidable.',
            'latitud' => '-33.4378',
            'longitud' => '-70.638',
            'slogan' => 'La mejor posada de la isla de Margarita',
            'imagen_principal' => 'imagen-portada.jpg',

        ]);

        Contact::create([
            'valor' => '+56 9 9999 9999',
            'tipo' => Contact::TYPE_PHONE,
            
        ]);
        
        Contact::create([
            'valor' => 'reservas@lasmarites.com',
            'tipo' => Contact::TYPE_EMAIL,
        ]);

        SocialNetwork::create([
            'nombre' => 'facebook',
            'enlace' => 'https://www.facebook.com/lasmarites',
            'icono' => 'facebook',
        ]);

        SocialNetwork::create([
            'nombre' => 'instagram',
            'enlace' => 'https://www.instagram.com/lasmarites',
            'icono' => 'instagram',
        ]);

        SocialNetwork::create([
            'nombre' => 'twitter',
            'enlace' => 'https://twitter.com/lasmarites',
            'icono' => 'twitter',
        ]);

        Gallery::create([
            'imagen' => 'room-1.jpg',
        ]);

        Gallery::create([
            'imagen' => 'room-2.jpg',
        ]);

        Gallery::create([
            'imagen' => 'room-3.jpg',
        ]);

        Gallery::create([
            'imagen' => 'room-4.jpg',
        ]);

        Gallery::create([
            'imagen' => 'room-5.jpg',
        ]);

        Gallery::create([
            'imagen' => 'room-6.jpg',
        ]);

        Gallery::create([
            'imagen' => 'room-7.jpg',
        ]);
    }
}
