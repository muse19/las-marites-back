<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescripcionReferenciaToCuentasCobrarPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cuentas_cobrar_pagos', function (Blueprint $table) {
            $table->string('descripcion', 500)->nullable()->after('metodo_pago_id');
            $table->string('referencia', 20)->nullable()->after('descripcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cuentas_cobrar_pagos', function (Blueprint $table) {
            $table->dropColumn('descripcion');
            $table->dropColumn('referencia');
        });
    }
}
