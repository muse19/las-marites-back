<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHabitacionReservaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitacion_reserva', function (Blueprint $table) {
            $table->unsignedBigInteger('habitacion_id');
            $table->unsignedBigInteger('reservacion_id');
            $table->date('fecha_llegada');
            $table->date('fecha_salida');
            $table->decimal('precio', 12,2);

            $table->foreign('habitacion_id')->references('id')->on('habitaciones');
            $table->foreign('reservacion_id')->references('id')->on('reservaciones');

            $table->primary(['habitacion_id', 'fecha_llegada', 'fecha_salida'], 'habitacion_reserva_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitacion_reserva');
    }
}
