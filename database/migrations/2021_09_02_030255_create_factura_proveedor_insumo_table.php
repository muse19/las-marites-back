<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaProveedorInsumoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_proveedor_insumo', function (Blueprint $table) {
            $table->unsignedBigInteger('factura_proveedor_id');
            $table->unsignedBigInteger('insumo_id');
            $table->integer('cantidad');
            $table->decimal('costo', 12, 2);

            $table->foreign('factura_proveedor_id')->references('id')->on('facturas_proveedor');
            $table->foreign('insumo_id')->references('id')->on('insumos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_proveedor_insumo');
    }
}
