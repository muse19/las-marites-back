<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHabitacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitaciones', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('tipo_habitacion_id');
            $table->string('numero', 4);
            $table->string('nombre', 100);
            $table->decimal('precio', 12, 2);
            $table->enum('estado', ['activo', 'inactivo'])->default('inactivo');
            $table->timestamps();

            $table->foreign('tipo_habitacion_id')->references('id')->on('tipo_habitaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitaciones');
    }
}
