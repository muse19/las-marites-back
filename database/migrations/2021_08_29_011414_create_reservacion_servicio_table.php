<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservacionServicioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservacion_servicio', function (Blueprint $table) {
           $table->id();
           $table->unsignedBigInteger('reservacion_id');
           $table->unsignedBigInteger('servicio_id');
           $table->decimal('precio', 12, 2);
           $table->timestamps();

           $table->foreign('reservacion_id')->references('id')->on('reservaciones');
           $table->foreign('servicio_id')->references('id')->on('servicios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservacion_servicio');
    }
}
