<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanionReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acompañante_reservacion', function (Blueprint $table) {
            $table->unsignedBigInteger('acompañante_id');
            $table->unsignedBigInteger('reservacion_id');
            $table->timestamps();

            $table->foreign('acompañante_id')->references('id')->on('usuarios');
            $table->foreign('reservacion_id')->references('id')->on('reservaciones');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companion_reservation');
    }
}
