<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('reservacion_id');
            $table->decimal('monto', 12, 2);
            $table->enum('estado', ['anulado', 'pendiente', 'confirmado', 'entregado']);
            $table->string('numero', 100)->unique();
            $table->boolean('visto')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('reservacion_id')->references('id')->on('reservaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
