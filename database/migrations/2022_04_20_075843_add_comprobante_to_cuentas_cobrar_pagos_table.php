<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddComprobanteToCuentasCobrarPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cuentas_cobrar_pagos', function (Blueprint $table) {
            $table->string('comprobante', 50)->nullable()->after('referencia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cuentas_cobrar_pagos', function (Blueprint $table) {
            $table->dropColumn('comprobante');
        });
    }
}
