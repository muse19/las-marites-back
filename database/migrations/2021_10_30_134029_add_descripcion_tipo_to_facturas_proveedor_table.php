<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescripcionTipoToFacturasProveedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facturas_proveedor', function (Blueprint $table) {
            $table->string('descripcion')->nullable()->after('numero');
            $table->enum('tipo', ['producto', 'servicio'])->after('descripcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facturas_proveedor', function (Blueprint $table) {
            $table->dropColumn('descripcion');
            $table->dropColumn('tipo');
        });
    }
}
