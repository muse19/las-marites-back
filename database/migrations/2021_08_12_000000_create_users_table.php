<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('rol_id');
            $table->string('cedula', 30)->nullable();
            $table->string('nombre', 60);
            $table->string('apellido', 60);
            $table->string('direccion', 500)->nullable();
            $table->string('pais_id', 45)->nullable();
            $table->string('ciudad', 100)->nullable();
            $table->enum('nacionalidad', ['v', 'e'])->default('v');
            $table->string('codigo_pais', 10)->nullable();
            $table->string('telefono', 11)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('rol_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
