<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuentasCobrarPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas_cobrar_pagos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cuenta_cobrar_id');
            $table->unsignedBigInteger('metodo_pago_id');
            $table->decimal('monto', 12,2);
            $table->timestamps();

            $table->foreign('cuenta_cobrar_id')->references('id')->on('cuentas_cobrar');
            $table->foreign('metodo_pago_id')->references('id')->on('metodos_pago');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas_cobrar_pagos');
    }
}
