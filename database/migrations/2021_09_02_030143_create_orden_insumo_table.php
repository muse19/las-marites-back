<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdenInsumoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_insumo', function (Blueprint $table) {
            $table->unsignedBigInteger('orden_compra_id');
            $table->unsignedBigInteger('insumo_id');
            $table->integer('cantidad');
            $table->decimal('costo', 12, 2);

            $table->foreign('orden_compra_id')->references('id')->on('ordenes_compra');
            $table->foreign('insumo_id')->references('id')->on('insumos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_insumo');
    }
}
