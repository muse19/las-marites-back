<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComestiblePedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comestible_pedido', function (Blueprint $table) {
            $table->unsignedBigInteger('comestible_id');
            $table->unsignedBigInteger('pedido_id');
            $table->integer('cantidad');
            $table->decimal('precio', 12, 2);

            $table->foreign('comestible_id')->references('id')->on('comestibles');
            $table->foreign('pedido_id')->references('id')->on('pedidos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_pedido');
    }
}
