<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>FACTURA</title>
	<style>
		* {
			margin: 0;
			padding: 0;
			box-sizing: border-box;
			font-family: sans-serif;
			text-transform: uppercase;
		}

		body,
		page {
			margin: 10px;
            position: relative;
		}

		table {
			width: 100%;
			border-collapse: collapse;
		}

		.text-right {
			text-align: right;
		}

		.text-center {
			text-align: center;
		}
        
        .table-logo {
            margin-bottom: 20px;
        }

        .table-logo tr td:nth-child(2){
            width: 70px;
        }
	 
		.header-text{
			text-align: center;
			font-size: 12px;
		}
		
		
		.client-text{
			font-size: 12px;
		}
		
		.invoice-title{
			text-align: center;
			margin-top: 20px;
			text-decoration: underline;
			font-weight: bold;
		}
		.separator{
			border-bottom: 3px dotted #000;
			margin: 15px 0px;
		}
		
		.item td{
			font-size: 12px;
		}
		.item.header td{
			font-weight: bold;
			padding-bottom: 5px;
		}
		.item.total td{
			font-size: 14px;
			font-weight: bold;
			padding-bottom: 5px;
		}
		.item td:nth-child(2){
			text-align: right;
		}
        
        tr.item.orders td:nth-child(2){
            text-align: center;
        }
        
        tr.item.orders td:nth-child(3){
            text-align: right;
        }        
		
		.footer {
			font-size: 14px;
		}
	</style>
</head>

<body>
    <table class="table-logo">
        <tr>
            <td>&nbsp;</td>
            <td>
                <img src="{{ storage_path('logo.png') }}" alt="" class="logo" height="70" width="70">
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
	<div class="header-text">Local posada turistica las marites</div>
	<div class="header-text">RIF. J-31269365-7</div>
	<div class="header-text">Calle principal cruz del pastel</div>
	<div class="header-text">estado nueva esparta</div>
	<div class="header-text">tlf. 0295-4170897 / 0424-8282806</div>
	
	<br>

	<div class="client-text">RIF/CI: {{$data->customer->cedula}}</div>
	<div class="client-text">Razon social: {{$data->customer->nombre}} {{$data->customer->apellido}}</div>
	
	
	<div class="invoice-title">factura</div>
	<br>
	
	<table>
		<tr class="item">
			<td>Código</td>
			<td style="text-transform: revert">{{$data->localizador}}</td>
		</tr>
		<tr class="item">
			<td>Fecha: {{ \Carbon\Carbon::now()->format('d-m-Y') }}</td>
			<td>Hora: {{ \Carbon\Carbon::now()->format('h:i') }}</td>
		</tr>
	</table>
	
	<div class="separator"></div>
    
    @if($data->rooms->count() > 0)
	<table>
		<tr class="item header">
			<td>Hospedaje</td>
			<td>Costo</td>
		</tr>
        @foreach($data->rooms as $room)
            <tr class="item">
                <td>{{$room->nombre}} - {{$room->type->nombre}}</td>
                <td>${{$room->pivot->precio}}</td>
            </tr>
        @endforeach
	</table>
	<div class="separator"></div>
    @endif
	

    @php
        $dishes = $data->orders->pluck('dishes')->collapse()->groupBy('nombre');
        
    @endphp

    @if($data->services->count() > 0)
	<table>
		<tr class="item header">
			<td>Servicios</td>
			<td>Costo</td>
		</tr>
        @foreach($data->services as $service)
            <tr class="item">
                <td>{{$service->nombre}}</td>
                <td>${{$service->pivot->precio}}</td>
            </tr>
		@endforeach
	</table>
	<div class="separator"></div>
    @endif
	
	@if($dishes->count() > 0)
	<table>
		<tr class="item header orders">
			<td>Plato</td>
            <td>Cantidad</td>
			<td>Costo</td>
		</tr>
        @foreach($dishes as $dish)
        <tr class="item orders">
            <td>{{$dish->first()->nombre}}</td>
            <td><span style="text-transform: lowercase">x</span>{{$dish->sum('pivot.cantidad')}}</td>
            <td>${{$dish->sum('pivot.precio')}}</td>
        </tr>
        @endforeach
	</table>	
	<div class="separator"></div>
    @endif
	
	<table>
		<tr class="item total">
			<td>total:</td>
			<td>${{ $data->total }}</td>
		</tr>
	</table>


	<br>
	<br>
	<br>
	<br>

	<div class="footer text-center">
		Gracias por visitarnos
	</div>
</body>

</html>
