<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<style>
		*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
	font-family: sans-serif;
}
		body,
page{
	margin: 2.58cm;
}
table{
	width: 100%;
	border-collapse: collapse;
}

.text-right{
	text-align: right;
}
.text-center{
	text-align: center;
}

.title{
	text-align: center;
	font-weight: bold;
	text-transform: uppercase;
	text-decoration: underline;
	margin: 40px 0;
}
.sub-title{
	margin: 20px 0;
}

.item{
	margin-bottom: 10px;
}
.item .item-label{
	font-size: 12px;
}
.table-items{
	max-width: 400px;
	margin: 0 auto;
}
.table-items td{
	padding: 10px 15px;
    border: 1px solid #2B2B2B;
}

.table-items tr:nth-child(odd) {
  background: #E9E9E9;
}

.table-items tr:not(.table-header) td{
	border: 1px solid #2B2B2B;
}
.table-header td{
	border: 1px solid #2B2B2B;
	background-color: #C4C4C4;
}


.footer{
	font-size: 14px;
}
	</style>
</head>

<body>
	<table>
		<tr>
			<td>
				<table>
					<tr>
						<td style="width:75px">
							<img src="{{ storage_path('logo.png') }}" alt="" height="70">
						</td>
						<td>
							<h5>Posada</h5>
							<h4>Las Marites</h4>
							<h6>RIF. J-31269365-7</h6>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<h3 class="text-right">Nro. 1524628</h3>
				<h4 class="text-right">18 Febrero 2022</h4>
			</td>
		</tr>
	</table>

	<div class="title">Solicitud de compra</div>

	<div class="sub-title">Datos del proveedor</div>

	<table>
		<tr>
			<td>
				<div class="item">
					<div class="item-label">Rif/Cedula</div>
					<div class="item-value">{{$data->supplier->rif}}</div>
				</div>
			</td>
			<td>
				<div class="item">
					<div class="item-label">Nombre/Razón social</div>
					<div class="item-value">{{$data->supplier->razon_social}}</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="item">
					<div class="item-label">Correo electrónico</div>
					<div class="item-value">{{$data->supplier->rif}}</div>
				</div>
			</td>
			<td>
				<div class="item">
					<div class="item-label">Teléfono</div>
					<div class="item-value">{{$data->supplier->telefono}}</div>
				</div>
			</td>
		</tr>
	</table>

	<br>
	<br>
	<br>
	<div class="table-title text-center">Listado de productos</div>
	<br>
	<table class="table-items">
		<tr class="table-header">
			<td>Descripción</td>
			<td class="text-center" style="width: 100px">Cantidad</td>
		</tr>
        @foreach($data->inputs as $input)
		<tr class="table-body">
			<td>{{$input->nombre}}</td>
			<td class="text-center">{{$input->pivot->cantidad}} {{$input->unit->simbolo}}</td>
		</tr>
		@endforeach
	</table>

	<br>
	<br>
	<br>
	<br>
	<br>

	<div class="footer text-center">
		Calle principal Cruz del Pastel, Local Posada Turistica Las Marites, Edo. Nueva Esparta <br>
		Tlf. 0295-4170891 / 0424-8282806
	</div>
</body>

</html>
