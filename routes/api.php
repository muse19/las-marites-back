<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\TestEventController;
use App\Http\Controllers\Admin\DishController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\RoomController;
use App\Http\Controllers\Admin\UnitController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\InputController;
use App\Http\Controllers\Web\PaymentController;
use App\Http\Controllers\Admin\StatusController;
use App\Http\Controllers\Admin\BookingController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\CalendarController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CurrencyController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\FacilityController;
use App\Http\Controllers\Admin\RoomTypeController;
use App\Http\Controllers\Admin\SupplierController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Web\RoomBookingController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\TrasactionController;
use App\Http\Controllers\Admin\DeleteImageController;
use App\Http\Controllers\Admin\BookingOrderController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\RequestInputController;
use App\Http\Controllers\Admin\CancelBookingController;
use App\Http\Controllers\Admin\PaymentMethodController;
use App\Http\Controllers\Admin\PurchaseOrderController;
use App\Http\Controllers\Admin\SocialNetworkController;
use App\Http\Controllers\Restaurant\SeeOrderController;
use App\Http\Controllers\Web\CustomerBookingController;
use App\Http\Controllers\Admin\BookingPaymentController;
use App\Http\Controllers\Admin\BookingServiceController;
use App\Http\Controllers\Admin\CheckInBookingController;
use App\Http\Controllers\Admin\ClassificationController;
use App\Http\Controllers\Admin\ConfirmPaymentController;
use App\Http\Controllers\Admin\CheckOutBookingController;
use App\Http\Controllers\Admin\SupplierInvoiceController;
use App\Http\Controllers\Admin\BookingCompanionController;
use App\Http\Controllers\Admin\ChangeStatusDishController;
use App\Http\Controllers\Admin\ChangeStatusRoomController;
use App\Http\Controllers\Admin\RoomAvailabilityController;
use App\Http\Controllers\Restaurant\CancelOrderController;
use App\Http\Controllers\Restaurant\ConfirmOrderController;
use App\Http\Controllers\Restaurant\DeliverOrderController;
use App\Http\Controllers\Restaurant\PendingOrderController;
use App\Http\Controllers\Admin\CompanionAvailableController;
use App\Http\Controllers\Admin\PurchaseOrderToPDFController;
use App\Http\Controllers\Web\CustomerRegistrationController;
use App\Http\Controllers\Admin\BookingInvoiceToPDFController;
use App\Http\Controllers\Admin\ChangeStatusBookingController;
use App\Http\Controllers\Web\RoomController as WebRoomController;
use App\Http\Controllers\Admin\BookingAccountReceivableController;
use App\Http\Controllers\Admin\BookingCompanionCapacityController;
use App\Http\Controllers\Web\CompanyController as WebCompanyController;
use App\Http\Controllers\Web\BookingRoomController as WebBookingRoomController;
use App\Http\Controllers\Web\RoomAvailabilityController as WebRoomAvailability;
use App\Http\Controllers\Web\BookingOrderController as WebBookingOrderController;
use App\Http\Controllers\Web\BookingBalanceController as WebBookingBalanceController;
use App\Http\Controllers\Web\BookingPaymentController as WebBookingPaymentController;
use App\Http\Controllers\Web\BookingServiceController as WebBookingServiceController;
use App\Http\Controllers\Web\BookingCompanionController as WebBookingCompanionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('admin/auth/login', [AuthController::class, 'login']);

Route::post('auth/login', [AuthController::class, 'login']);

Route::middleware(['auth:api'])->group(function (){

    Route::prefix('admin')->group(function (){
        Route::delete('auth/logout', [AuthController::class, 'logout']);
        Route::get('auth/me', [AuthController::class, 'me']);

        Route::apiResource('room-types', RoomTypeController::class);

        Route::apiResource('rooms', RoomController::class);
        Route::patch('rooms/{roomId}/status', ChangeStatusRoomController::class);

        Route::apiResource('facilities', FacilityController::class);
        Route::apiResource('customers', CustomerController::class);
        Route::apiResource('bookings', BookingController::class);

        Route::get('statuses', StatusController::class);

        Route::get('room-availability', RoomAvailabilityController::class);

        Route::apiResource('bookings.companions', BookingCompanionController::class);

        Route::patch('booking/{bookingId}/status', ChangeStatusBookingController::class);

        Route::apiResource('services', ServiceController::class);

        Route::apiResource('bookings.services', BookingServiceController::class);

        Route::apiResource('inputs', InputController::class);

        Route::post('request-inputs', RequestInputController::class);

        Route::apiResource('suppliers', SupplierController::class);

        Route::apiResource('purchase-orders', PurchaseOrderController::class);

        Route::apiResource('purchase-invoices', SupplierInvoiceController::class);

        Route::apiResource('dishes', DishController::class);

        Route::patch('dishes/{dishesId}/status', ChangeStatusDishController::class);

        Route::get('transactions', TrasactionController::class);

        Route::apiResource('categories', CategoryController::class);

        Route::get('menu', MenuController::class);

        Route::apiResource('bookings.orders', BookingOrderController::class);

        Route::apiResource('companies', CompanyController::class)->only(['index', 'store']);

        Route::apiResource('contacts', ContactController::class);

        Route::apiResource('social-networks', SocialNetworkController::class);

        Route::apiResource('gallery', GalleryController::class)->only(['index', 'store', 'destroy']);

        Route::delete('images/{imageName}', DeleteImageController::class);

        Route::get('payment-methods', PaymentMethodController::class);

        Route::apiResource('bookings.payments', BookingPaymentController::class);

        Route::patch('payments/{paymentId}/confirm', ConfirmPaymentController::class);

        Route::get('dashboard', DashboardController::class);

        Route::get('bookings/{bookingId}/balance', BookingAccountReceivableController::class);

        Route::patch('bookings/{bookingId}/check-in', CheckInBookingController::class);

        Route::patch('bookings/{bookingId}/check-out', CheckOutBookingController::class);

        Route::patch('bookings/{bookingId}/cancel', CancelBookingController::class);

        Route::get('bookings/{bookingId}/companion-available', CompanionAvailableController::class);

        Route::get('bookings/{bookingId}/companion-capacity', BookingCompanionCapacityController::class);

        Route::post('calendar', CalendarController::class);

        Route::get('units', UnitController::class);

        Route::apiResource('users', UserController::class);

        Route::apiResource('classifications', ClassificationController::class);
        Route::apiResource('roles', RoleController::class);
        Route::apiResource('permissions', PermissionController::class);

        Route::get('notifications', NotificationController::class);
    });

    Route::apiResource('customer/bookings', CustomerBookingController::class, ['only' => ['index', 'show']]);

    Route::get('booking/{bookingId}/companions', WebBookingCompanionController::class);

    Route::apiResource('booking/{bookingId}/orders', WebBookingOrderController::class, ['only' => ['index', 'store']]);

    Route::get('booking/{bookingId}/services', WebBookingServiceController::class);

    Route::get('booking/{bookingId}/payments', WebBookingPaymentController::class);

    Route::get('booking/{bookingId}/rooms', WebBookingRoomController::class);

    Route::get('booking/{bookingId}/balance', WebBookingBalanceController::class);

});

Route::get('purchase-orders/{purchaseOrderId}/pdf', PurchaseOrderToPDFController::class);
Route::get('booking-invoice/{bookingId}/pdf', BookingInvoiceToPDFController::class);

Route::get('room-availability', WebRoomAvailability::class);

Route::get('company-info', WebCompanyController::class);

Route::apiResource('rooms', WebRoomController::class, ['only' => ['index', 'show']]);

Route::get('countries', CountryController::class);

Route::post('bookings', RoomBookingController::class);

Route::prefix('restaurant')->group(function (){
  Route::get('pending-orders', PendingOrderController::class);
  Route::apiResource('confirmed-orders', ConfirmOrderController::class, ['only' => ['index', 'update']]);
  Route::apiResource('cancelled-orders', CancelOrderController::class, ['only' => ['index', 'update']]);
  Route::apiResource('completed-orders', DeliverOrderController::class, ['only' => ['index', 'update']]);
  Route::patch('orders/{orderId}/see', SeeOrderController::class);

});

Route::post('payments/pay', [PaymentController::class, 'pay'])->name('pay');
Route::get('payments/approval', [PaymentController::class, 'approval'])->name('approval');

Route::post('register-customer', CustomerRegistrationController::class);

Route::apiResource('currencies', CurrencyController::class);

Route::get('test-event', TestEventController::class);
