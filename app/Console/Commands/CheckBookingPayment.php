<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Booking;
use Illuminate\Console\Command;

class CheckBookingPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:check-payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Anula las reservaciones que no tengan pagos realizados en el lapso establecido.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Booking::where('created_at', '<', Carbon::now()->subHours(24)->toDateTimeString())
          ->whereHas('statuses', function($q) {
            $q->where('status_id', 1);
          })
          ->get()
          ->map(function($item){
            return $item->statuses()->attach(3);
          });
    }
}
