<?php

namespace App\Reports;

use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Response;

class DompdfReport implements ReportInterface
{

    private $pdf;

    public function __construct(PDF $pdf)
    {
        $this->pdf = $pdf;
    }

    public function fromRequest($data, $view) : ReportInterface
    {
        $this->pdf->loadView($view, ['data' => $data]);
        return $this;
    }

    public function download($filename): Response
    {
        return $this->pdf->download($filename);
    }

    public function stream($filename): Response
    {
        return $this->pdf->stream($filename);
    }

    public function output(){
        return $this->pdf->output();
    }
}
