<?php

namespace App\Reports;

use Illuminate\Http\Response;

interface ReportInterface
{
    public function fromRequest($data, $view): ReportInterface;

    public function download($filename): Response;

    public function stream($filename): Response;

    public function output();
}
