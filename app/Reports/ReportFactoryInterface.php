<?php

namespace App\Reports;

use InvalidArgumentException;

interface ReportFactoryInterface
{
    public function create($type): ReportInterface;
}
