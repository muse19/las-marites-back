<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $table = 'empresas';

    protected $fillable = ['nombre', 'slogan', 'imagen_principal','logo', 'sobre_nosotros', 'direccion', 'latitud', 'longitud'];

    public function getLogoAttribute($value){
        return !$value ? null :  url('/img/'. $value) ;
    }

    public function getImagenPrincipalAttribute($value){
        return !$value ? null :  url('/img/'. $value) ;
    }
}
