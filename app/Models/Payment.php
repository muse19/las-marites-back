<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $table = 'cuentas_cobrar_pagos';

    protected $fillable = ['cuenta_cobrar_id', 'metodo_pago_id', 'descripcion','referencia','monto', 'comprobante', 'verificado'];

    public function paymentMethod(){
        return $this->belongsTo(PaymentMethod::class, 'metodo_pago_id');
    }

    public function accountReceivable(){
        return $this->belongsTo(AccountReceivable::class, 'cuenta_cobrar_id');
    }
}
