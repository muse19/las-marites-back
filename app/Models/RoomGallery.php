<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomGallery extends Model
{
    use HasFactory;

    protected $table = 'habitacion_fotos';
    protected $fillable = ['habitacion_id', 'imagen'];

    public function getImagenAttribute($value){
        return url('/img/'. $value) ;
    }
}
