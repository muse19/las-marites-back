<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'reservaciones';

    protected $fillable = [
        'usuario_id', 
        'estado_id',
        'localizador', 
        'fecha_llegada', 
        'fecha_salida',
        'checkin',
        'checkout',
        'cantidad_personas', 
        'total', 
        'observaciones'
    ];

    protected $dates = ['deleted_at'];

    public function rooms(){
        return $this->belongsToMany(Room::class, 'habitacion_reserva', 'reservacion_id', 'habitacion_id')
            ->withPivot('precio', 'fecha_llegada', 'fecha_salida')
            ->withTrashed();
    }

    public function status(){
        return $this->belongsTo(Status::class, 'estado_id');
            
    }

    public function customer(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function companions(){
        return $this->belongsToMany(User::class, 'acompañante_reservacion', 'reservacion_id', 'acompañante_id')
            ->withTimestamps();
    }

    public function services(){
        return $this->belongsToMany(Service::class, 'reservacion_servicio', 'reservacion_id', 'servicio_id')
           ->using(BookingServicePivot::class)
            ->withTimestamps()
            ->withPivot('id','precio')
            ->withTrashed();
    }

    public function orders(){
        return $this->hasMany(Order::class, 'reservacion_id');
    }

    public function accountReceivable(){
        return $this->hasOne(AccountReceivable::class, 'reservacion_id');
    }

    public static function calculateTotal($rooms, $checkIn, $checkOut){
        return $rooms->sum('precio') * Carbon::parse($checkIn)->diffInDays($checkOut);
    }

    public static function getCompanionMaxCapacity($bookingId){
        $rooms = Booking::where('id', $bookingId)
            ->with('rooms.type')
            ->firstOrFail()
            ->rooms;

        return $rooms->sum('type.capacidad') -1;
    }
}
