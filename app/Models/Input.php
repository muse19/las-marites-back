<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Input extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'insumos';
    protected $fillable = ['unidad_id', 'nombre', 'descripcion', 'cantidad', 'cantidad_min', 'cantidad_max', 'costo'];
    protected $dates = ['deleted_at'];

    public function transactions(){
        return $this->hasMany(Transaction::class, 'insumo_id');
    }

    public static function getQuantityInOrder($data, $id) : int
    {
        return $data[array_search($id, array_column($data, 'id'))]['cantidad'];
    }

    public static function getCostInOrder($data, $id) : int
    {
        return $data[array_search($id, array_column($data, 'id'))]['costo'];
    }

    public function unit(){
        return $this->belongsTo(Unit::class, 'unidad_id');
    }
}
