<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'categorias';

    protected $fillable = ['nombre'];

    protected $dates = ['deleted_at'];

    public function dishes(){
        return $this->hasMany(Dish::class, 'categoria_id');
    }
}
