<?php

namespace App\Models;

use App\Traits\HasRoleAndPermissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use HasFactory, HasRoleAndPermissions, SoftDeletes;

    protected $table = 'usuarios';

    protected $fillable = [
        'nombre',
        'apellido',
        'direccion',
        'telefono',
        'codigo_pais',
        'cedula',
        'rol_id',
        'pais_id',
        'nacionalidad',
        'ciudad'
    ];

    protected $dates = ['deleted_at'];

    public function account() {
        return $this->hasOne(Account::class, 'usuario_id');
    }

    public function bookings(){
        return $this->hasMany(Booking::class, 'usuario_id');
    }

}
