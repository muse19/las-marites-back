<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;

    protected $table = 'permisos';


    protected $fillable = [
        'nombre',
        'clasificacion_id',
        'slug'
    ];

    public function classification()
    {
    return $this->belongsTo(Classification::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'permiso_role', 'permiso_id', 'rol_id');
    }
}
