<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class Account extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    protected $table = 'cuenta';

    protected $fillable = ['usuario_id', 'correo', 'contrasenia'];

    public function user() {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAuthPassword() {
        return $this->contrasenia;
    }
}
