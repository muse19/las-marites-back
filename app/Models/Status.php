<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    const PENDING   = 1;
    const PAYMENT_UNVERIFIED = 2;
    const CONFIRMED = 3;
    const CANCELED  = 4;
    const CHECK_IN  = 5;
    const CHECK_OUT = 6;

    protected $table = 'estados';

    protected $fillable = ['nombre'];

}
