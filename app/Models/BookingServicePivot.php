<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class BookingServicePivot extends Pivot
{
    use HasFactory;

    protected $table = 'reservacion_servicio';

    public $primaryKey   = 'id';

    public $incrementing = true;
}
