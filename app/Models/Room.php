<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use HasFactory, SoftDeletes;

    const ENABLE = 'activo';
    const DISABLE = 'inactivo';

    protected $table = 'habitaciones';
    protected $fillable = ['tipo_habitacion_id', 'numero', 'nombre', 'precio', 'descripcion','estado'];

    protected $dates = ['deleted_at'];

    public function facilities() {
        return $this->belongsToMany(Facility::class, 'caracteristica_habitacion', 'habitacion_id', 'caracteristica_id');
    }

    public function pictures() {
        return $this->hasMany(RoomGallery::class, 'habitacion_id');
    }

    public function type() {
        return $this->belongsTo(RoomType::class, 'tipo_habitacion_id');
    }

    public function bookings(){
        return $this->belongsToMany(Booking::class, 'habitacion_reserva', 'habitacion_id', 'reservacion_id');
    }

    public static function checkAvailability(array $rooms, $from, $to)
    {
        $availableRooms = Room::whereDoesntHave('bookings', function ($q) use ($from, $to) {
            $q->where(function ($q) use ($from, $to) {
                $q->where('reservaciones.fecha_llegada', '<=', $from)
                    ->where('reservaciones.fecha_salida', '>', $from);
            })
            ->orWhere(function ($q) use ($from, $to) {
                $q->where('reservaciones.fecha_llegada', '<', $to)
                    ->where('reservaciones.fecha_salida', '>=', $to);
            })
            ->orWhere(function ($q) use ($from, $to) {
                $q->where('reservaciones.fecha_llegada', '>=', $from)
                    ->where('reservaciones.fecha_salida', '<=', $to);
            });
        })
            ->whereIn('id', $rooms)
            ->where('estado', Room::ENABLE)
            ->select('id')
            ->get();

        return $availableRooms;
    }

}
