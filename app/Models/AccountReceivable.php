<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountReceivable extends Model
{
    use HasFactory;

    protected $table = 'cuentas_cobrar';

    protected $fillable = ['usuario_id', 'reservacion_id', 'saldo'];

    public function payments(){
        return $this->hasMany(Payment::class, 'cuenta_cobrar_id');
    }

    public function booking(){
        return $this->belongsTo(Booking::class, 'reservacion_id');
    }
}
