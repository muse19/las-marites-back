<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    use HasFactory, SoftDeletes;

    const COMPLETED = '1';
    const PENDING = '0';

    protected $table = 'ordenes_compra';
    protected $fillable = ['proveedor_id', 'numero', 'monto_aprox', 'estado'];
    protected $dates = ['deleted_at'];

    public function inputs(){
        return $this->belongsToMany(Input::class, 'orden_insumo', 'orden_compra_id', 'insumo_id')
            ->withPivot( 'cantidad', 'costo');
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class, 'proveedor_id')->withTrashed();
    }

    public static function calculateTotal($inputs, $inputsOrder) {

        $total = 0;

        foreach ($inputs as $input) {

            $quantity = Input::getQuantityInOrder($inputsOrder, $input->id);
            $cost = $input->costo;

            $total += $quantity * $cost;
        }

        return $total;
    }
}
