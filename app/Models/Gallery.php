<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;

    protected $table = 'galeria';

    protected $fillable = ['imagen'];

    public function getImagenAttribute($value){
        return url('/img/'. $value) ;
    }
}
