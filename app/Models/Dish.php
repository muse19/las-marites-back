<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dish extends Model
{
    use HasFactory, SoftDeletes;

    const STATUS_ACTIVE = 'activo';
    const STATUS_INACTIVE = 'inactivo';

    protected $table = 'comestibles';

    protected $fillable = ['categoria_id','nombre', 'descripcion', 'imagen', 'precio'];

    protected $dates = ['deleted_at'];

    public function category(){
        return $this->belongsTo(Category::class, 'categoria_id')->withTrashed();
    }

    public function getImagenAttribute($value){
        return url('/img/'. $value) ;
    }

    public static function getQuantityInOrder($data, $id) : int
    {
        return $data[array_search($id, array_column($data, 'id'))]['cantidad'];
    }
}
