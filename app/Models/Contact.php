<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    const TYPE_EMAIL = 'correo';
    const TYPE_PHONE = 'telefono';

    protected $table = 'contactos';

    protected $fillable = ['valor', 'tipo'];
}
