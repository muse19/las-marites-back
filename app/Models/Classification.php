<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classification extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'clasificaciones';

    protected $fillable = ['nombre'];

    protected $dates = ['deleted_at'];

    public function permissions()
    {
      return $this->hasMany(Permission::class, 'clasificacion_id');
    }
}
