<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

  const TYPE_NEW_BOOKING = 'new_booking';
  const TYPE_CANCELLED_BOOKING = 'cancelled_booking';
  const TYPE_PAYMENT_RECEIVED = 'payment_received';

    protected $table = 'notificaciones';

    protected $fillable = [
        'tipo',
        'usuario_id',
        'mensaje',
        'data',
        'visto',
    ];
}
