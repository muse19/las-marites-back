<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    const STATUS_PENDING = 'pendiente';
    const STATUS_CONFIRMED = 'confirmado';
    const STATUS_DELIVERED = 'entregado';
    const STATUS_CANCELLED = 'anulado';

    protected $table = 'pedidos';

    protected $fillable = ['reservacion_id', 'numero', 'monto', 'estado', 'visto'];

    protected $dates = ['deleted_at'];

    public function dishes(){
        return $this->belongsToMany(Dish::class, 'comestible_pedido', 'pedido_id', 'comestible_id')
            ->withPivot('cantidad', 'precio')
            ->withTrashed();
    }

    public function booking(){
        return $this->belongsTo(Booking::class, 'reservacion_id')->withTrashed();
    }

    public static function calculateTotal($dishes, $dishesOrder) {

        $total = 0;

        foreach ($dishes as $dish) {

            $quantity = Dish::getQuantityInOrder($dishesOrder, $dish->id);
            $price = $dish->precio;

            $total += $quantity * $price;
        }

        return $total;
    }

}
