<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    const ENTRY = '1';
    const OUT = '0';

    protected $table = 'transacciones';
    protected $fillable = ['insumo_id', 'usuario_id', 'cantidad', 'tipo'];

    public function input(){
        return $this->belongsTo(Input::class, 'insumo_id')->withTrashed();
    }

    public function user(){
        return $this->belongsTo(User::class, 'usuario_id')->withTrashed();
    }

    public function getTipoAttribute($value){
        return $value ? 'Entrada' : 'Salida';
    }

}
