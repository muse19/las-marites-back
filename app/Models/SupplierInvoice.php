<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierInvoice extends Model
{
    use HasFactory, SoftDeletes;

    const TYPE_PRODUCT = 'producto';
    const TYPE_SERVICE = 'servicio';

    protected $table = 'facturas_proveedor';
    protected $fillable = ['proveedor_id', 'numero', 'descripcion','tipo','total', 'fecha'];
    protected $dates = ['deleted_at'];

    public function inputs(){
        return $this->belongsToMany(Input::class, 'factura_proveedor_insumo', 'factura_proveedor_id', 'insumo_id')
            ->withPivot('costo', 'cantidad');
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class, 'proveedor_id')->withTrashed();
    }
}
