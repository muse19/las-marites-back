<?php

namespace App\Services;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Traits\ConsumesExternalServices;

class StripeService
{
    use ConsumesExternalServices, ApiResponser;

    protected $baseUri;
    protected $key;
    protected $secret;
    protected $bookingService;

    public function __construct(BookingService $bookingService)
    {
        $this->baseUri = config('services.stripe.base_uri');
        $this->key = config('services.stripe.key');
        $this->secret = config('services.stripe.secret');
        $this->bookingService = $bookingService;
    }

    public function resolveAuthorization(&$queryParams, &$formParams, &$headers)
    {
      $headers['Authorization'] = $this->resolveAccessToken();
    }

    public function resolveAccessToken()
    {
       return "Bearer {$this->secret}";
    }

    public function decodeResponse($response)
    {
        return json_decode($response);
    }

    public function handlePayment(Request $request){

      $intent = $this->createIntent(100, $request->payment_method);

      $this->handleApproval($intent->id, $request->booking_id);

    }

    public function handleApproval($paymentIntentId, $bookingId){


        $confirmation = $this->confirmPayment($paymentIntentId);

        if($confirmation->status === 'requires_action'){

          $clientSecret = $confirmation->client_secret;

          return  $clientSecret;
        }

        if($confirmation->status === 'succeeded'){

          $paymentData = [
             'amount' => $confirmation->amount,
             'reference' => $confirmation->id,
             'payment_method' => 'stripe',
          ];

          $this->bookingService->confirmBooking($bookingId, $paymentData);

          return $this->showResponse(['message' => 'Pago confirmado']);
        }


        return $this->errorResponse('No podemos confirmar tu pago', 401);
    }

    public function createIntent($value, $paymentMethod){


        return $this->makeRequest(
        'POST',
        '/v1/payment_intents',
        [],
        [
          'amount' => round($value * 100),
          'currency' => 'usd',
          'payment_method_types' => ['card'],
          'payment_method_data' => [
            'type' => 'card',
            'card' => [
                'token' => $paymentMethod
            ]
          ],
          'confirmation_method' => 'manual',
        ],
        [],
        $isJsonRequest = false
      );
    }

    public function confirmPayment($paymentIntentId){

      return $this->makeRequest(
        'POST',
        "/v1/payment_intents/{$paymentIntentId}/confirm",
      );
    }
}
