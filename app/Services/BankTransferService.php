<?php

namespace App\Services;

use App\Models\Booking;
use App\Models\Payment;
use App\Helpers\ImageStore;
use Illuminate\Http\Request;
use App\Models\PaymentMethod;
use App\Events\PaymentReceived;
use App\Models\Status;
use Exception;
use Illuminate\Support\Facades\DB;

class BankTransferService
{

    public function handlePayment(Request $request){

        $booking = Booking::where('localizador', $request->booking_id)
            ->with('accountReceivable')
            ->firstOrFail();

        DB::beginTransaction();

        try{

            $paymentMethod = PaymentMethod::where('id', $request->payment_platform_id)->firstOrFail();

            $imageName = ImageStore::saveImage($request->receipt);

            $payment = Payment::create([
                'cuenta_cobrar_id' => $booking->accountReceivable->id,
                'metodo_pago_id' => $paymentMethod->id,
                'referencia' => $request->reference,
                'comprobante' => $imageName,
                'monto' => $booking->total
            ]);

            $payment = [
                'localizador' => $booking->localizador,
                'monto' => $payment->monto,
                'referencia' => $payment->referencia
            ];

            DB::commit();

            $booking->update(['estado_id' => Status::PAYMENT_UNVERIFIED]);

            event(new PaymentReceived($payment));

            return $payment;

        }catch(\Exception $e){
            DB::rollBack();
            throw new Exception($e);
        }

    }

}
