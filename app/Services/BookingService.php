<?php

namespace App\Services;

use App\Events\ConfirmedBooking;
use App\Models\Booking;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\Models\Status;

class BookingService
{

    public function getBooking($bookingId)
    {
        $booking = Booking::where('localizador', $bookingId)
            ->with('accountReceivable')
            ->firstOrFail();

        return $booking;
    }

    public function confirmBooking($bookingId, $data)
    {
        $booking = $this->getBooking($bookingId);

        $paymentId = PaymentMethod::where('nombre', $data['payment_method'])->first()->id;

        Payment::create([
            'cuenta_cobrar_id' => $booking->accountReceivable->id,
            'metodo_pago_id' =>$paymentId,
            'referencia' => $data['reference'],
            'monto' => $data['amount'],
        ]);

        $booking->update(['estado_id' => Status::CONFIRMED]);

        //event(new ConfirmedBooking($booking));

        return $booking;
    }

}
