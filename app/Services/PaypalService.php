<?php

namespace App\Services;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Traits\ConsumesExternalServices;

class PaypalService
{
    use ConsumesExternalServices, ApiResponser;

    protected $baseUri;
    protected $clientId;
    protected $clientSecret;
    protected $bookingService;

    public function __construct(BookingService $bookingService)
    {
        $this->baseUri = config('services.paypal.base_uri');
        $this->clientId = config('services.paypal.client_id');
        $this->clientSecret = config('services.paypal.client_secret');
        $this->bookingService = $bookingService;
    }

    public function resolveAuthorization(&$queryParams, &$formParams, &$headers)
    {
        $headers['Authorization'] = $this->resolveAccessToken();
    }

    public function resolveAccessToken()
    {
        $credentials = base64_encode("{$this->clientId}:{$this->clientSecret}");

        return "Basic {$credentials}";
    }

    public function decodeResponse($response)
    {
        return json_decode($response);
    }

    public function handlePayment(Request $request){

      $order = $this->createOrder($request->value, $request->booking_id);

      $orderLinks = collect($order->links);

      $approve = $orderLinks->where('rel', 'approve')->first();

      return ['url' => $approve->href];

    }

    public function handleApproval($request){

        $payment = $this->capturePayment($request->token);

        $paymentData = [
          'amount' => $payment->purchase_units[0]->payments->captures[0]->amount->value,
          'reference' => $payment->id,
          'payment_method' => 'Paypal',
        ];

        $this->bookingService->confirmBooking($request->bookingId, $paymentData);


        return view('payment-success');

    }

    public function createOrder($value, $bookingId){


      return $this->makeRequest(
        'POST',
        '/v2/checkout/orders',
        [],
        [
          'intent' => 'CAPTURE',
          'purchase_units' => [
            0 => [
              'amount' => [
                'currency_code' => 'USD',
                'value' => round($value,2)
              ]
            ]
          ],
          'application_context' => [
            'brand_name' => config('app.name'),
            'shipping_preference' => 'NO_SHIPPING',
            'user_action' => 'PAY_NOW',
            'return_url' => route('approval', ['bookingId' => $bookingId])
          ]
        ],
        [],
        $isJsonRequest = true
      );
    }

    public function capturePayment($approvalId){

      return $this->makeRequest(
        'POST',
        "/v2/checkout/orders/{$approvalId}/capture",
        [],
        [],
        [
          'Content-Type' => 'application/json',
        ]
      );

    }

}
