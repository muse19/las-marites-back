<?php

namespace App\Traits;

use App\Models\Role;
use App\Models\Permission;

trait HasRoleAndPermissions
{

    public function role()
    {
        return $this->belongsTo(Role::class, 'rol_id');
    }

    public function hasRole($roles)
    {
        foreach ($roles as $role){
            if ($this->role->slug === $role) return true ;
        }

        return false;
    }

    public function hasPermissionTo($permission)
    {
        return $this->hasPermissionThroughRole($permission);
    }

    protected function hasPermissionThroughRole($permission)
    {
        $permission = Permission::where('slug', $permission)->first();

        if ($permission) {
            foreach ($permission->roles as $role) {
                if ($this->role->name === $role->name) return true;
            }
        }

        return false;
    }

    public function getPermissions()
    {
        $permissions = $this->role->permissions->pluck('slug');

        $data = (object)[];

        foreach ($permissions as $permission) {
            $data->$permission = true;
        }

        return $data;
    }
}

