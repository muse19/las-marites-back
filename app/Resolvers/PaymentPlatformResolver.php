<?php

namespace App\Resolvers;

class PaymentPlatformResolver
{
    protected $paymentPlatforms;

    public function __construct(){
      
      $this->paymentPlatforms = [
        1 => 'paypal',
        2 => 'stripe',
        3 => 'bank_transfer'
      ];
    }

    public function resolveService($paymentPlatform){
      
      $name = $this->paymentPlatforms[$paymentPlatform];

      $service = config("services.{$name}.class");

      if($service){
        return resolve($service);
      }

      throw new \Exception("La plataforma seleccionada no se encuentra configurada");
    }
}