<?php

namespace App\Observers;

use App\Models\AccountReceivable;
use App\Models\Booking;

class BookingObserver
{
    public $afterCommit = true;

    public function created(Booking $booking)
    {
        AccountReceivable::create([
            'usuario_id' => $booking->usuario_id,
            'reservacion_id' => $booking->id,
            'saldo' => $booking->total
        ]);
    }


    public function updated(Booking $booking)
    {
        //
    }

    public function deleted(Booking $booking)
    {
        //
    }

    public function restored(Booking $booking)
    {
        //
    }

    public function forceDeleted(Booking $booking)
    {
        //
    }
}
