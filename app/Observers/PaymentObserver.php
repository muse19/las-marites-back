<?php

namespace App\Observers;

use App\Models\AccountReceivable;
use App\Models\Payment;

class PaymentObserver
{

    public function created(Payment $payment)
    {
        AccountReceivable::where('id', $payment->cuenta_cobrar_id)
            ->decrement('saldo', $payment->monto);
    }


    public function updated(Payment $payment)
    {
        //
    }


    public function deleted(Payment $payment)
    {
        AccountReceivable::where('id', $payment->cuenta_cobrar_id)
            ->increment('saldo', $payment->monto);
    }


    public function restored(Payment $payment)
    {
        //
    }


    public function forceDeleted(Payment $payment)
    {
        //
    }
}
