<?php

namespace App\Observers;

use App\Models\AccountReceivable;
use App\Models\BookingServicePivot;

class BookingServicePivotObserver
{

    public function created(BookingServicePivot $bookingServicePivot)
    {
        AccountReceivable::where('reservacion_id', $bookingServicePivot->reservacion_id)
            ->increment('saldo', $bookingServicePivot->precio);
    }


    public function updated(BookingServicePivot $bookingServicePivot)
    {
        //
    }


    public function deleted(BookingServicePivot $bookingServicePivot)
    {
        AccountReceivable::where('reservacion_id', $bookingServicePivot->reservacion_id)
            ->decrement('saldo', $bookingServicePivot->precio);
    }


    public function restored(BookingServicePivot $bookingServicePivot)
    {
        //
    }


    public function forceDeleted(BookingServicePivot $bookingServicePivot)
    {

    }
}
