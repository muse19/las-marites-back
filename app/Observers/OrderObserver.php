<?php

namespace App\Observers;

use App\Models\AccountReceivable;
use App\Models\Order;

class OrderObserver
{

    public function created(Order $order)
    {
        AccountReceivable::where('reservacion_id', $order->reservacion_id)
            ->increment('saldo', $order->monto);
    }


    public function updated(Order $order)
    {
        if($order->estado === Order::STATUS_CANCELLED && $order->getOriginal('estado') !== Order::STATUS_CANCELLED){
            AccountReceivable::where('reservacion_id', $order->reservacion_id)
                ->decrement('saldo', $order->monto);
        }
    }


    public function deleted(Order $order)
    {

    }

    public function restored(Order $order)
    {
        //
    }


    public function forceDeleted(Order $order)
    {
        //
    }
}
