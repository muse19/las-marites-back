<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'numero' => $this->numero,
            'monto' => $this->monto,
            'estado' => $this->estado,
            'fecha' => $this->created_at,
            'comestibles' => $this->dishes->map(function($item){
                return [
                  'id' => $item->id,
                  'nombre' => $item->nombre,
                  'imagen' => $item->imagen,
                  'precio' => $item->pivot->precio,
                  'cantidad' => $item->pivot->cantidad
                ];
            })
        ];
    }
}
