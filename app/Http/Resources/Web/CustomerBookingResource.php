<?php

namespace App\Http\Resources\Web;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerBookingResource extends JsonResource
{

    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'localizador' => $this->localizador,
        'fecha_llegada' => $this->fecha_llegada,
        'fecha_salida' => $this->fecha_salida,
        'total' => $this->total,
        'cantidad_habitaciones' => $this->rooms_count,
        'cantidad_acompanantes' => $this->companions_count,
        'estado' => [
            'nombre' => $this->status->nombre,
            'color' => $this->status->color
        ],
        'fecha_creacion' => $this->created_at,
      ];
    }
}
