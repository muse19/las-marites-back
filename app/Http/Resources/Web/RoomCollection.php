<?php

namespace App\Http\Resources\Web;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RoomCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(function ($item){
           return [
               'id' => $item->id,
               'nombre' => $item->nombre,
               'precio' => $item->precio,
               'tipo' => [
                   'nombre' => $item->type->nombre,
                   'capacidad' => $item->type->capacidad,
               ],
               'caracteristicas' => $item->facilities->map(function($item){
                   return [
                       'nombre' => $item->nombre,
                       'icono' => $item->icono
                   ];
               }),
               'imagenes' => $item->pictures->map(function ($item){
                   return [
                       'imagen' => $item->imagen
                   ];
               })
           ];
        });
    }
}
