<?php

namespace App\Http\Resources\Web;

use Illuminate\Http\Resources\Json\JsonResource;

class RoomResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'tipo' => [
                'nombre' => $this->type->nombre,
                'capacidad' => $this->type->capacidad,
            ],
            'caracteristicas' => $this->facilities->map(function($item){
                return [
                    'nombre' => $item->nombre,
                    'icono' => $item->icono
                ];
            }),
            'imagenes' => $this->pictures->map(function ($item){
                return [
                  'imagen' => $item->imagen
                ];
            })
        ];
    }
}
