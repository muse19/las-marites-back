<?php

namespace App\Http\Resources\Web;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RoomListCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return $this->collection->map(function ($item){
            return [
                'id' => $item->id,
                'nombre' => $item->nombre,
                'precio' => $item->precio,
                'capacidad' => $item->type->capacidad,
                'imagen' => $item->pictures->first()->imagen,
                'caracteristicas' => $item->facilities->map(function($facility){
                    return [
                        'nombre' => $facility->nombre,
                        'icono' => $facility->icono
                    ];
                })
            ];
        });
    }
}
