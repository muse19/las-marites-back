<?php

namespace App\Http\Resources\Web;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CustomerBookingCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return $this->collection->map(function($item){
          return [
            'localizador' => $item->localizador,
            'fecha_llegada' => $item->fecha_llegada,
            'fecha_salida' => $item->fecha_salida,
            'total' => $item->total,
            'cantidad_habitaciones' => $item->rooms_count,
            'cantidad_acompanantes' => $item->companions_count,
            'fecha_creacion' => $item->created_at,
            'estado' => [
              'nombre' => $item->status->nombre,
              'color' => $item->status->color
            ]
          ];
        });
    }
}
