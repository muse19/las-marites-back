<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RoomResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tipo_habitacion_id' => $this->tipo_habitacion_id,
            'numero'=>  $this->numero,
            'nombre' => $this->nombre,
            'precio'=>  $this->precio,
            'descripcion' => $this->descripcion,
            'estado' => $this->estado,
            'caracteristicas' => $this->facilities->pluck('id'),
            'imagenes' => $this->pictures->map(function($item){
                return [
                  'id' => $item->id,
                  'imagen' => $item->imagen
                ];
            })
        ];
    }
}
