<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $countries = json_decode(file_get_contents(base_path('public/countries.json')), true);

        $country = $this->pais_id 
                    ? $countries[array_search($this->pais_id, array_column($countries, 'id'))]
                    : null;
        
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'apellido' => $this->apellido,
            'nacionalidad' => $this->nacionalidad,
            'cedula' => $this->cedula,
            'correo' => $this->account->correo,
            'codigo_pais' => $this->codigo_pais,
            'telefono' => $this->telefono,
            'direccion' => $this->direccion,
            'ciudad' => $this->ciudad,
            'pais_id' => $this->pais_id,
            'pais' => [
                'nombre' => $country['name'] ?? null,
                'bandera' => $country['flag'] ?? null
              ],
            'reservaciones' => $this->bookings->map(function($booking){
                return [
                    'id' => $booking->id,
                    'localizador' => $booking->localizador,
                    'fecha_llegada' => $booking->fecha_llegada,
                    'fecha_salida' => $booking->fecha_salida,
                    'total' => $booking->total,
                    'created_at' => $booking->created_at,
                    'current_status' => [
                        'id' => $booking->status->id,
                        'nombre' => $booking->status->nombre,
                        'color' => $booking->status->color,
                        'created_at' => $booking->status->created_at,
                    ],
                ];
            }),

        ];
    }
}
