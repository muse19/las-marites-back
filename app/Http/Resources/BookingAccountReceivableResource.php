<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookingAccountReceivableResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'reservacion' => [
                'fecha_llegada' => $this->fecha_llegada,
                'fecha_salida' => $this->fecha_salida,
                'total' => $this->total,
                'habitaciones' => $this->rooms->map(function($item){
                    return [
                        'nombre' => $item->nombre,
                        'precio' => $item->pivot->precio,
                        'tipo' => $item->type->nombre
                    ];
                }),
            ],

            'servicios' => [
                'lista' => $this->services->map(function($item){
                    return [
                        'nombre' => $item->nombre,
                        'precio' => $item->pivot->precio,
                        'fecha' => $item->pivot->created_at
                    ];
                }),
                'total' => $this->services->sum('pivot.precio')
            ],
            'pedidos' => [
                'lista' => $this->orders->map(function($item){
                    return [
                        'numero' => $item->numero,
                        'monto' => $item->monto,
                        'feche' => $item->created_at,
                        'estado' => $item->estado
                    ];
                }),
                'total' => $this->orders->sum('monto')
            ],
            'balance' => $this->accountReceivable->saldo
        ];
    }
}
