<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CustomerCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $countries = json_decode(file_get_contents(base_path('public/countries.json')), true);
        
        return $this->collection->map(function ($customer) use ($countries) {

            $country = $customer->pais_id 
                  ? $countries[array_search($customer->pais_id, array_column($countries, 'id'))]
                  : null;

            return [
                'id' => $customer->id,
                'nacionalidad' => $customer->nacionalidad,
                'cedula' => $customer->cedula,
                'nombre' => $customer->nombre,
                'apellido' => $customer->apellido,
                'pais' => [
                    'nombre' => $country['name'] ?? null,
                    'bandera' => $country['flag'] ?? null
                ],
                'fecha_registro' => $customer->created_at,
                'cant_reservaciones' => $customer->cant_reservaciones,
                'ultima_reservacion' => $customer->cant_reservaciones ? $customer->bookings->first()->created_at : null,
                
            ];
        });
    }
}
