<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{

    public function toArray($request)
    {
        return $this->collection->map(function ($item){
            return [
                'numero' => $item->numero,
                'monto' => $item->monto,
                'estado' => $item->estado,
                'fecha' => $item->created_at,
                'cant_comestibles' => $item->dishes->count(),
                'comestibles' => $item->dishes->map(function($dish) {
                    return [
                        'nombre' => $dish->nombre,
                        'descripcion' => $dish->description,
                        'precio' => $dish->pivot->precio,
                        'cantidad' => $dish->pivot->cantidad,
                    ];
                })
            ];
        });
    }
}
