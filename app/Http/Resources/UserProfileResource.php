<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileResource extends JsonResource
{

    public function toArray($request)
    {
      $countries = json_decode(file_get_contents(base_path('public/countries.json')), true);

      $country = $this->pais_id
                  ? $countries[array_search($this->pais_id, array_column($countries, 'id'))]
                  : null;

      return [
          'id' => $this->id,
          'nombre' => $this->nombre,
          'apellido' => $this->apellido,
          'correo' => $this->account->correo,
          'telefono' => $this->telefono,
          'direccion' => $this->direccion,
          'ciudad' => $this->ciudad,
          'codigo_pais' => $this->codigo_pais,
          'pais_id' => $this->pais_id,
          'pais' => [
            'nombre' => $country['name'] ?? null,
            'bandera' => $country['flag'] ?? null
          ],
          'nacionalidad' => $this->nacionalidad,
          'cedula' => $this->cedula,
          'fecha_registro' => $this->created_at,
          'cant_reservaciones' => $this->bookings->count(),
        ];
    }
}
