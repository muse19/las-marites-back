<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\ConfirmedBooking;

class TestEventController extends Controller
{
    public function __invoke()
    {
        $booking = ['id' => 1];
        
        event(new ConfirmedBooking($booking));

        return $booking;
    }
}
