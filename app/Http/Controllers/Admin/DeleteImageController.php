<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageStore;
use App\Http\Controllers\ApiController;
use App\Models\RoomGallery;

class DeleteImageController extends ApiController
{
    public function __invoke($imageName){

        $result = ImageStore::deleteImage($imageName);

        if(!$result){
            return $this->errorResponse('Ha ocurrido un error', 409);
        }

        RoomGallery::where('imagen', $imageName)
            ->first()
            ->delete();

        return $this->showResponse($result, 204);
    }
}
