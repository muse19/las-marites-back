<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Unit;


class UnitController extends ApiController
{
    public function __invoke(){

        $units = Unit::select('id', 'nombre', 'simbolo')->get();

        return $this->showResponse($units);
    }
}
