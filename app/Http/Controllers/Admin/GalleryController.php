<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageStore;
use App\Http\Controllers\ApiController;
use App\Http\Requests\GalleryStoreRequest;
use App\Models\Gallery;
use Illuminate\Http\Request;

class GalleryController extends ApiController
{

    public function index()
    {
        $galleries = Gallery::select('id', 'imagen')->get();

        return $this->showResponse($galleries);
    }


    public function store(GalleryStoreRequest $request)
    {
        foreach ($request->pictures as $picture){
            $imageName = ImageStore::saveImage($picture);

            $picture = Gallery::create([
                'imagen' => $imageName
            ]);
        }

        return $this->showResponse($picture, 201);
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($galleryId)
    {
        $gallery = Gallery::findOrFail($galleryId);

        ImageStore::deleteImage($gallery->getRawOriginal('imagen'));

        $gallery->delete();

        return $this->showResponse($gallery, 204);
    }
}
