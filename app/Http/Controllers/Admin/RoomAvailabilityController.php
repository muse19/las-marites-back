<?php

namespace App\Http\Controllers\Admin;

use App\Models\Room;
use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class RoomAvailabilityController extends ApiController
{
    public function __invoke(Request $request){
        $from = $request->from;
        $to = $request->to;

        $rooms = Room::whereDoesntHave('bookings', function($q) use ($from, $to){
            $q->where('estado_id', '!=', Status::CANCELED)
                ->where(function($q) use ($from, $to) {
                    $q->where('reservaciones.fecha_llegada', '<=', $from)
                        ->where('reservaciones.fecha_salida', '>', $from);
                    })
                    ->orWhere(function($q) use ($from, $to){
                        $q->where('reservaciones.fecha_llegada', '<', $to)
                            ->where('reservaciones.fecha_salida', '>=', $to);
                    })
                    ->orWhere(function($q) use ($from, $to){
                        $q->where('reservaciones.fecha_llegada', '>=', $from)
                            ->where('reservaciones.fecha_salida', '<=', $to);
                    });
                })
            ->where('estado', Room::ENABLE)
            ->with('type')
            ->get();

        return $this->showResponse($rooms);
    }
}
