<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookingServiceStoreRequest;
use App\Http\Requests\ServiceUpdateRequest;
use App\Models\Booking;
use App\Models\BookingServicePivot;
use App\Models\Service;


class BookingServiceController extends ApiController
{
    public function index($bookingId)
    {
        $services = Booking::where('id', $bookingId)
            ->with('services:id,nombre')
            ->firstOrFail()
            ->services;

        return $this->showResponse($services);
    }

    public function store(BookingServiceStoreRequest $request, $bookingId)
    {
        $booking = Booking::findOrFail($bookingId);
        $service = Service::findOrFail($request->servicio_id);

        $booking->services()->attach([
            $service->id => ['precio' => $service->precio]
        ]);

        return $this->showResponse($service,201);
    }

    public function show($id)
    {
        //
    }

    public function update(ServiceUpdateRequest $request, $serviceId)
    {

    }

    public function destroy($bookingId, $serviceId)
    {
        $booking = Booking::findOrFail($bookingId);

        BookingServicePivot::where('reservacion_id', $booking->id)
            ->where('id', $serviceId)
            ->first()
            ->delete();

        return $this->showResponse($booking, 204);
    }
}
