<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceStoreRequest;
use App\Http\Requests\ServiceUpdateRequest;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends ApiController
{
    public function index()
    {
        $services = Service::select('id', 'nombre', 'precio')->get();

        return $this->showResponse($services);
    }

    public function store(ServiceStoreRequest $request)
    {
        $service = Service::create($request->only('nombre', 'precio'));

        return $this->showResponse($service,201);
    }

    public function show($id)
    {
        //
    }

    public function update(ServiceUpdateRequest $request, $serviceId)
    {

        $service = Service::findOrFail($serviceId);

        $service->update($request->only('nombre', 'precio'));

        return $this->showResponse($service, 204);
    }

    public function destroy($serviceId)
    {
        $service = Service::findOrFail($serviceId);

        $service->delete();

        return $this->showResponse($service, 204);
    }
}
