<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookingCompanionStoreRequest;
use App\Models\Booking;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class BookingCompanionController extends ApiController
{

    public function index($bookingId)
    {
        $companions = Booking::where('id', $bookingId)
            ->with('companions:id,nacionalidad,cedula,nombre,apellido')
            ->first()
            ->companions;

        return $this->showResponse($companions);
    }


    public function store(BookingCompanionStoreRequest $request, $bookingId)
    {
        $booking = Booking::findOrFail($bookingId);

        if($booking->companions->count() >= Booking::getCompanionMaxCapacity($booking->id)){
           return $this->errorResponse(['companion' => 'Límite de acompañantes alcanzado'], 422 );
        }

        $roleId = Role::where('slug', 'customer')->first()->id;

        $companion = User::firstOrCreate(
            ['cedula' => $request->cedula],
            ['nombre' => $request->nombre, 'apellido' => $request->apellido, 'rol_id' => $roleId ]
        );

        $booking->companions()->attach($companion->id);

        return $this->showResponse($companion, 201);
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($bookingId, $companionId)
    {
        $booking = Booking::findOrFail($bookingId);

        $booking->companions()->detach($companionId);

        return $this->showResponse($booking, 204);
    }
}
