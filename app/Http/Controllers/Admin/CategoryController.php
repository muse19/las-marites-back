<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Models\Category;


class CategoryController extends ApiController
{

    public function index()
    {
        $categories = Category::select('id', 'nombre')->get();

        return $this->showResponse($categories);
    }

    public function store(CategoryStoreRequest $request)
    {
        $category = Category::create($request->only('nombre'));

        return $this->showResponse($category, 201);
    }

    public function show($id)
    {
        //
    }

    public function update(CategoryUpdateRequest $request, $categoryId)
    {
        $category = Category::findOrFail($categoryId);

        $category->update($request->only('nombre'));

        return $this->showResponse($category, 204);
    }

    public function destroy($categoryId)
    {
        $category = Category::findOrFail($categoryId);

        $category->delete();

        return $this->showResponse($category, 204);
    }
}
