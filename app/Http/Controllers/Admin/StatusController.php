<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Status;
use Illuminate\Http\Request;

class StatusController extends ApiController
{
    public function __invoke(){
        $statuses = Status::select('id', 'nombre', 'color')->get();

        return $this->showResponse($statuses);
    }
}
