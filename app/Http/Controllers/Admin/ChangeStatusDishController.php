<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Dish;

class ChangeStatusDishController extends ApiController
{

    public function __invoke($dishId)
    {
        $dish = Dish::findOrFail($dishId);

        $status = $dish->estado === Dish::STATUS_ACTIVE ? Dish::STATUS_INACTIVE : Dish::STATUS_ACTIVE;

        $dish->estado = $status;
        $dish->save();

        return $this->showResponse($dish, 204);
    }

}
