<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Booking;
use App\Models\Status;

class CancelBookingController extends ApiController
{
    public function __invoke($bookingId){

        $booking = Booking::findOrFail($bookingId);

        $booking->update(['estado_id' => Status::CANCELED]);

        return $this->showResponse($booking, 204);
    }
}
