<?php

namespace App\Http\Controllers\Admin;

use App\Models\Booking;
use App\Reports\ReportFactory;
use App\Http\Controllers\Controller;

class BookingInvoiceToPDFController extends Controller
{
    public function __invoke($bookingId)
    {
        $booking = Booking::where('id', $bookingId)
            ->with('rooms.type', 'services', 'customer:id,nombre,apellido,cedula','orders.dishes', 'accountReceivable:id,reservacion_id,saldo')
            ->first();

        $report = (new ReportFactory(app()))->create('dompdf');

        return $report->fromRequest($booking, 'pdf.booking-invoice')->stream("factura-". $booking->localizador . ".pdf");
    }
}
