<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Requests\BookingStoreRequest;
use App\Models\Booking;
use App\Models\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class BookingController extends ApiController
{

    public function index()
    {
        $bookings = Booking::with('customer', 'status')->get();

        return $this->showResponse($bookings);
    }


    public function store(BookingStoreRequest $request)
    {
        DB::beginTransaction();

        $priceRooms = [];

        $rooms = Room::whereIn('id', $request->habitaciones)
            ->select('id', 'precio')
            ->get();

        $roomsAvailable = Room::checkAvailability($request->habitaciones, $request->fecha_llegada, $request->fecha_salida);

        if (!$roomsAvailable->count() === count($request->habitaciones)) {

            $data = [
                'message' => 'Habitaciones no disponibles.',
                'rooms' => array_values(array_diff($request->habitaciones, $roomsAvailable->pluck('id')->toArray()))
            ];

            return $this->errorResponse($data, 409);
        }

        $total = Booking::calculateTotal($rooms, $request->fecha_llegada, $request->fecha_salida);

        try {
            $booking = Booking::create([
                'localizador' => Str::random(9),
                'estado_id' => $request->estado_id,
                'usuario_id' => $request->usuario_id,
                'fecha_llegada' => $request->fecha_llegada,
                'fecha_salida' => $request->fecha_salida,
                'cantidad_personas' => $request->cantidad_personas,
                'total' => $total
            ]);

            foreach ($rooms as $room){
                array_push($priceRooms, [
                    'precio' => $room->precio,
                    'fecha_llegada' => $request->fecha_llegada,
                    'fecha_salida' => $request->fecha_salida
                ]);
            }

            $data = array_combine($rooms->pluck('id')->toArray(), $priceRooms);

            $booking->rooms()->sync($data);

            DB::commit();

        }catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e, 500);
        }

        return  $this->showResponse($booking, 201);
    }


    public function show($id)
    {
        $booking = Booking::where('id', $id)
            ->with('customer', 'rooms.type', 'status')->first();

        return $this->showResponse($booking);
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
