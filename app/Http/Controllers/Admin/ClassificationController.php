<?php

namespace App\Http\Controllers\Admin;

use App\Models\Classification;
use App\Http\Controllers\ApiController;
use App\Http\Requests\ClassificationStoreRequest;
use App\Http\Requests\ClassificationUpdateRequest;

class ClassificationController extends ApiController
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function index()
  {
    $classifications = Classification::all();

    return $this->showResponse($classifications);
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClassificationStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
  public function store(ClassificationStoreRequest $request)
  {
    $classification = Classification::create($request->all());

    return $this->showResponse($classification, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

    /**
     * Update the specified resource in storage.
     *
     * @param ClassificationUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
  public function update(ClassificationUpdateRequest $request, $id)
  {
    $classification = Classification::findOrFail($id);

    $classification->update($request->all());

    return $this->showResponse($classification, 204);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy($id)
  {
    $classification = Classification::findOrFail($id);

    $classification->delete();

    return $this->showResponse($classification, 204);
  }
}
