<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageStore;
use App\Http\Controllers\ApiController;
use App\Http\Requests\DishStoreRequest;
use App\Http\Requests\DishUpdateRequest;
use App\Models\Dish;
use Illuminate\Support\Facades\DB;

class DishController extends ApiController
{

    public function index()
    {
        $dishes = Dish::select('id', 'categoria_id' ,'nombre', 'descripcion', 'imagen', 'precio','estado')
            ->with('category:id,nombre')
            ->get();

        return $this->showResponse($dishes);
    }


    public function store(DishStoreRequest $request)
    {
        DB::beginTransaction();

        try {
            $dish = Dish::create($request->only('categoria_id','nombre', 'descripcion', 'precio'));

            $imageName = ImageStore::saveImage($request->imagen);

            $dish->imagen = $imageName;
            $dish->save();

            DB::commit();

        }catch (\Exception $e){
            DB::rollBack();

            ImageStore::deleteImage($imageName);

            return $this->errorResponse($e->getMessage(), 500);
        }

        return $this->showResponse($dish, 201);
    }


    public function show($id)
    {
        //
    }


    public function update(DishUpdateRequest $request, $dishId)
    {
        DB::beginTransaction();

        try {
            $dish = Dish::findOrFail($dishId);

            $dish->fill($request->only('categoria_id','nombre', 'descripcion', 'precio'));

            if($request->filled('imagen')){
                $imageName = ImageStore::saveImage($request->imagen);

                ImageStore::deleteImage($dish->getRawOriginal('imagen'));

                $dish->imagen = $imageName;
            }

            $dish->save();

            DB::commit();

        }catch (\Exception $e){
            DB::rollBack();

            ImageStore::deleteImage($imageName);

            return $this->errorResponse($e->getMessage(), 500);
        }

        return $this->showResponse($dish, 204);
    }


    public function destroy($dishId)
    {
        $dish = Dish::findOrFail($dishId);

        $dish->delete();

        return $this->showResponse($dish, 204);
    }
}
