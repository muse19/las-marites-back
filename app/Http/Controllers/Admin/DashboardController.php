<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Booking;
use App\Models\Room;
use App\Models\Status;
use Carbon\Carbon;


class DashboardController extends ApiController
{
    public function __invoke(){

        $arrivals = Booking::with('customer', 'rooms:id', 'status')
            ->whereDate('fecha_llegada', Carbon::now())
            ->where('estado_id', Status::CONFIRMED)
            ->get();

        
        $departs = Booking::with('customer', 'rooms:id', 'statuses')
            ->whereDate('fecha_salida', Carbon::now())
            ->where('estado_id', Status::CHECK_IN)
            ->get();


        $today =  Booking::with('customer', 'rooms:id', 'statuses')
            ->withCount('companions')
            ->whereDate('fecha_salida', '!=' , Carbon::now())
            ->where('estado_id', Status::CHECK_IN)
            ->get();

        $total = Room::all()->count();

        $occupancy = collect($today)->pluck('rooms')->collapse()->count() + collect($departs)->pluck('rooms')->collapse()->count();
        
        $customers = $today->sum('companions_count') + $today->count();

        $data = (object)[
            'llegadas' => $arrivals,
            'salidas' => $departs,
            'hoy' => $today,
            'metricas' => (object)[
                'total' => $total,
                'ocupacion' => $occupancy,
                'clientes' => $customers
            ]
        ];

        return $this->showResponse($data);
    }
}
