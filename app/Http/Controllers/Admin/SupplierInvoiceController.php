<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Requests\SupplierInvoiceStoreRequest;
use App\Models\Input;
use App\Models\SupplierInvoice;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupplierInvoiceController extends ApiController
{

    public function index()
    {
        $purchaseInvoices = SupplierInvoice::select('id','proveedor_id','numero', 'total', 'tipo', 'descripcion','created_at', 'fecha')
            ->with('inputs:id,unidad_id,nombre', 'supplier:id,rif,razon_social', 'inputs.unit')
            ->get();

        return $this->showResponse($purchaseInvoices);
    }


    public function store(SupplierInvoiceStoreRequest $request)
    {
        DB::beginTransaction();

        try{
            $inputs = [];
            $total = $request->tipo === SupplierInvoice::TYPE_SERVICE ? $request->total : 0;

            $purchaseInvoice = SupplierInvoice::create([
                'proveedor_id' => $request->proveedor_id,
                'numero' => $request->numero,
                'tipo' => $request->tipo,
                'descripcion' => $request->descripcion,
                'total' => $total,
                'fecha' => $request->fecha ?? Carbon::now()->format('Y-m-d')
            ]);

            if($request->tipo === SupplierInvoice::TYPE_PRODUCT){
                $inputsInInvoice = Input::whereIn('id', array_column( $request->insumos, 'id'))
                    ->select('id','cantidad','costo')
                    ->get();

                foreach ($inputsInInvoice as $input){
                    $quantity = Input::getQuantityInOrder($request['insumos'], $input->id);
                    $cost = Input::getCostInOrder($request['insumos'], $input->id);

                    $input->update(['costo' => $cost, 'cantidad' => $input->cantidad + $quantity]);

                    $total += $quantity * $cost;

                    Transaction::create([
                        'insumo_id' => $input->id,
                        'cantidad' => $quantity,
                        'tipo' => Transaction::ENTRY
                    ]);

                    array_push($inputs, [
                        'cantidad' => $quantity,
                        'costo' => $cost
                    ]);
                }

                $inputs = array_combine($inputsInInvoice->pluck('id')->toArray(), $inputs);

                $purchaseInvoice->inputs()->sync($inputs);

                $purchaseInvoice->total = $total;
            }

            $purchaseInvoice->save();

            DB::commit();

        }catch (\Exception $e){
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }

        return $this->showResponse($purchaseInvoice, 201);

    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($purchaseInvoiceId)
    {
        $purchaseInvoice = SupplierInvoice::findOrFail($purchaseInvoiceId);

        $purchaseInvoice->delete();

        return $this->showResponse($purchaseInvoice, 204);
    }
}
