<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Requests\SupplierStoreRequest;
use App\Http\Requests\SupplierUpdateRequest;
use App\Models\Supplier;

class SupplierController extends ApiController
{

    public function index()
    {
        $providers = Supplier::select('id', 'rif', 'razon_social', 'correo', 'telefono', 'direccion')->get();

        return $this->showResponse($providers);
    }


    public function store(SupplierStoreRequest $request)
    {
        $provider = Supplier::create([
            'rif' => $request->rif,
            'razon_social' => $request->razon_social,
            'correo' => $request->correo,
            'telefono' => $request->telefono,
            'direccion' => $request->direccion
        ]);

        return $this->showResponse($provider, 201);
    }


    public function show($id)
    {
        //
    }

    public function update(SupplierUpdateRequest $request, $providerId)
    {
        $provider = Supplier::findOrFail($providerId);

        $provider->update($request->only('rif', 'razon_social', 'correo', 'telefono', 'direccion'));

        return $this->showResponse($provider, 204);
    }

    public function destroy($providerId)
    {
        $provider = Supplier::findOrFail($providerId);

        $provider->delete();

        return $this->showResponse($provider, 204);
    }
}
