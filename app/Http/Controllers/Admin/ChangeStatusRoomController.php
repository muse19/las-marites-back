<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Room;

class ChangeStatusRoomController extends ApiController
{
    public function __invoke($roomId){
        $room = Room::findOrFail($roomId);

        $status = $room->estado === Room::ENABLE ? Room::DISABLE : Room::ENABLE;

        $room->estado = $status;
        $room->save();

        return $this->showResponse($room, 204);
    }
}
