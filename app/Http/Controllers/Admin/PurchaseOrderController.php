<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Requests\PurchaseOrderStoreRequest;
use App\Http\Requests\PurchaseOrderUpdateRequest;
use App\Models\Input;
use App\Models\PurchaseOrder;
use Illuminate\Support\Facades\DB;

class PurchaseOrderController extends ApiController
{

    public function index()
    {
        $purchaseOrders = PurchaseOrder::with('inputs:id,unidad_id,nombre', 'inputs.unit', 'supplier')
            ->orderBy('created_at')
            ->get();

        return $this->showResponse($purchaseOrders);
    }


    public function store(PurchaseOrderStoreRequest $request)
    {
        DB::beginTransaction();

        $inputs = [];

        $inputsInOrder = Input::whereIn('id', array_column($request->insumos, 'id'))
            ->select('id', 'costo')
            ->get();

        $total = PurchaseOrder::calculateTotal($inputsInOrder, $request->insumos);

        try {

            foreach($inputsInOrder as $i => $input) {

                $quantity = Input::getQuantityInOrder($request['insumos'], $input->id);
                $cost = $input->costo;

                array_push($inputs, [
                    'cantidad' => $quantity,
                    'costo' => $cost
                ]);
            }

            $inputs = array_combine($inputsInOrder->pluck('id')->toArray(), $inputs);

            $purchaseOrder = PurchaseOrder::create([
                'proveedor_id' => $request->proveedor_id,
                'numero' => time(),
                'monto_aprox' => $total,
                'estado' => PurchaseOrder::PENDING
            ]);

            $purchaseOrder->inputs()->sync($inputs);

            DB::commit();

            return $this->showResponse($purchaseOrder, 201);

        }catch (\Exception $e){
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }

    }


    public function show($id)
    {
        //
    }


    public function update(PurchaseOrderUpdateRequest $request, $purchaseOrderId)
    {
        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);

        DB::beginTransaction();

        $inputs = [];

        $inputsInOrder = Input::whereIn('id', array_column($request->insumos, 'id'))
            ->select('id', 'costo')
            ->get();

        $total = PurchaseOrder::calculateTotal($inputsInOrder, $request->insumos);

        try {

            foreach($inputsInOrder as $i => $input) {

                $quantity = Input::getQuantityInOrder($request['insumos'], $input->id);
                $cost = $input->costo;

                array_push($inputs, [
                    'cantidad' => $quantity,
                    'costo' => $cost
                ]);
            }

            $inputs = array_combine($inputsInOrder->pluck('id')->toArray(), $inputs);

            $purchaseOrder->PurchaseOrder::update([
                'proveedor_id' => $request->proveedor_id,
                'monto_aprox' => $total,
            ]);

            $purchaseOrder->inputs()->sync($inputs);

            DB::commit();

            return $this->showResponse($purchaseOrder, 204);

        }catch (\Exception $e){
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }
    }

    public function destroy($purchaseOrderId)
    {
        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);

        $purchaseOrder->delete();

        return $this->showResponse($purchaseOrder, 204);
    }
}
