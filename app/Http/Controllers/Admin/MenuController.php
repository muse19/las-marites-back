<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Category;
use App\Models\Dish;

class MenuController extends ApiController
{

    public function __invoke()
    {
        $dishesByCategory = Category::select('id', 'nombre')
            ->with(['dishes' => function($q){
                return $q->where('estado', Dish::STATUS_ACTIVE)
                    ->select('id', 'categoria_id','nombre', 'descripcion', 'precio', 'imagen');
            }])
            ->get();

        return $this->showResponse($dishesByCategory);
    }


}
