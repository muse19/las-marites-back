<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Requests\RoomTypeStoreRequest;
use App\Http\Requests\RoomTypeUpdateRequest;
use App\Models\RoomType;

class RoomTypeController extends ApiController
{
    public function index()
    {
        $roomTypes = RoomType::all();

        return $this->showResponse($roomTypes);
    }

    public function store(RoomTypeStoreRequest $request)
    {
        $roomType = RoomType::create($request->only('nombre', 'capacidad'));

        return $this->showResponse($roomType, 201);
    }

    public function show($id)
    {
        //
    }


    public function update(RoomTypeUpdateRequest $request, $id)
    {
        $roomType = RoomType::findOrFail($id);

        $roomType->update($request->only('nombre','capacidad'));

        return $this->showResponse($roomType, 204);
    }


    public function destroy($id)
    {
        $roomType = RoomType::findOrFail($id);

        $roomType->delete();

        return $this->showResponse($roomType, 204);
    }
}
