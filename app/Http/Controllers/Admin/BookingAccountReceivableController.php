<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Resources\BookingAccountReceivableResource;
use App\Models\Booking;

class BookingAccountReceivableController extends ApiController
{
    public function __invoke($bookingId){

        $balance = Booking::where('id', $bookingId)
            ->with('rooms.type', 'services', 'orders', 'accountReceivable:id,reservacion_id,saldo')
            ->first();

        return $this->showResponse(new BookingAccountReceivableResource($balance));
    }
}
