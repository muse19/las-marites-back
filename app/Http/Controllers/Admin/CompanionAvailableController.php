<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Booking;
use App\Models\User;


class CompanionAvailableController extends ApiController
{
    public function __invoke($bookingId){

        $booking = Booking::findOrFail($bookingId);

        $companionsAvailable = User::whereDoesntHave('bookings', function($q) use ($booking){
            $q->where(function($q) use ($booking) {
                $q->where('fecha_llegada', '<=', $booking->fecha_llegada)
                    ->where('fecha_salida', '>', $booking->fecha_llegada);
            })
                ->orWhere(function($q) use ($booking){
                    $q->where('fecha_llegada', '<', $booking->fecha_salida)
                        ->where('fecha_salida', '>=', $booking->fecha_salida);
                })
                ->orWhere(function($q) use ($booking){
                    $q->where('fecha_llegada', '>=', $booking->fecha_llegada)
                        ->where('fecha_salida', '<=', $booking->fecha_salida);
                });
        })
        ->whereHas('role', function ($q){
            return $q->where('nombre', 'Cliente');
        })
        ->where('id', '!=', $booking->usuario_id)
        ->select('id', 'nacionalidad', 'cedula', 'nombre', 'apellido')
        ->get();

        return $this->showResponse($companionsAvailable);

    }
}
