<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\PaymentMethod;


class PaymentMethodController extends ApiController
{
    public function __invoke(){
        $paymentMethods = PaymentMethod::select('id', 'nombre')->get();

        return $this->showResponse($paymentMethods);
    }
}
