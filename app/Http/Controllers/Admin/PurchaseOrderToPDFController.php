<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Reports\ReportFactory;
use App\Http\Controllers\Controller;
use App\Models\PurchaseOrder;

class PurchaseOrderToPDFController extends Controller
{
    public function __invoke($purchaseOrderId)
    {
        
        $purchaseOrder = PurchaseOrder::with('inputs:id,unidad_id,nombre', 'inputs.unit', 'supplier')
                ->where('id', $purchaseOrderId)
                ->firstOrFail();
        
        $report = (new ReportFactory(app()))->create('dompdf');

        return $report->fromRequest($purchaseOrder, 'pdf.purchase-order')->stream("orden-compra-". str_pad((string)$purchaseOrder->id, 8, "0", STR_PAD_LEFT).".pdf");

    }
}
