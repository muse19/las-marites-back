<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageStore;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends ApiController
{

    public function index()
    {
        $company = Company::first();

        return $this->showResponse($company);
    }


    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $imageName = null;

            $company = Company::first();

            if(!$company){
                $company = Company::create($request->only('nombre', 'slogan','direccion', 'sobre_nosotros', 'latitud', 'longitud'));
            }else{
                $company->update($request->only('nombre', 'slogan','direccion', 'sobre_nosotros', 'latitud', 'longitud'));
            }

            if($request->filled('logo')){
                $imageName = ImageStore::saveImage($request->logo);

                ImageStore::deleteImage($company->getRawOriginal('logo'));

                $company->logo = $imageName;
            }

            if($request->filled('imagen_principal')){
                $imageName = ImageStore::saveImage($request->imagen_principal);

                ImageStore::deleteImage($company->getRawOriginal('imagen_principal'));

                $company->imagen_principal = $imageName;
            }

            $company->save();

            DB::commit();

        }catch (\Exception $e){
            DB::rollBack();

            ImageStore::deleteImage($imageName);

            return $this->errorResponse($e->getMessage(), 500);
        }

        return $this->showResponse($company, 201);

    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
