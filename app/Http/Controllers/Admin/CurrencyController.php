<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use App\Http\Controllers\ApiController;
use App\Http\Requests\CurrencyStoreRequest;
use App\Http\Requests\CurrencyUpdateRequest;

class CurrencyController extends ApiController
{
    
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    
    public function index()
    {
        $currencies = Currency::select('id', 'codigo', 'nombre', 'tasa')->get();

        return $this->showResponse($currencies);
    }

    public function store(CurrencyStoreRequest $request)
    {
        $currency = Currency::create($request->all());

        return $this->showResponse($currency, 201);
    }

    public function show($id)
    {
        //
    }

    public function update(CurrencyUpdateRequest $request, $id)
    {
        $currency = Currency::findOrFail($id);

        $currency->update($request->all());

        return $this->showResponse($currency, 204);
    }

    public function destroy($id)
    {
        $currency = Currency::findOrFail($id);

        $currency->delete();

        return $this->showResponse($currency, 204);
    }
}
