<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use App\Http\Controllers\ApiController;
use App\Http\Resources\CustomerResource;
use App\Http\Resources\CustomerCollection;
use App\Http\Requests\CustomerStoreRequest;
use App\Http\Requests\CustomerUpdateRequest;

class CustomerController extends ApiController
{
    public function index()
    {
        $customers = User::whereHas('role', function ($q){
            return $q->where('slug', 'customer');
        })
        ->withCount('bookings as cant_reservaciones')
        ->with(['bookings' => function($q){
            return $q->select('id', 'usuario_id', 'fecha_llegada', 'fecha_salida','created_at')
                ->orderBy('created_at', 'desc');

        }])
        ->get();

        return $this->showResponse(new CustomerCollection($customers));
    }

    public function store(CustomerStoreRequest $request)
    {
        $data = $request->all();
        $data['rol_id'] = Role::where('slug', 'customer')->first()->id;

        $customer = User::create($data);

        return $this->showResponse($customer,201);
    }

    public function show($id)
    {
        $customer = User::whereHas('role', function ($q){
            return $q->where('slug', 'customer');
        })
        ->where('id', $id)
        ->with(['bookings' => function($q){
            return $q->with('status')
                    ->orderBy('created_at', 'desc');
        },
        'account' => function($q){
            return $q->select('usuario_id', 'correo');
        }])
        ->firstOrFail();

        return $this->showResponse(new CustomerResource($customer));
    }

    public function update(CustomerUpdateRequest $request, $id)
    {
        $customer = User::findOrFail($id);

        $customer->update($request->all());

        return $this->showResponse($customer, 204);
    }

    public function destroy($id)
    {
        $customer = User::findOrFail($id);

        $customer->delete();

        return $this->showResponse($customer, 204);
    }
}
