<?php

namespace App\Http\Controllers\Admin;

use App\Models\Permission;
use App\Models\Classification;
use App\Http\Controllers\ApiController;
use App\Http\Requests\PermissionStoreRequest;
use App\Http\Requests\PermissionUpdateRequest;

class PermissionController extends ApiController
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function index()
  {
    $permissions = Classification::with('permissions')->get();

    return $this->showResponse($permissions);
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param PermissionStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
  public function store(PermissionStoreRequest $request)
  {
    $permission = Permission::create($request->all());

    return $this->showResponse($permission, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

    /**
     * Update the specified resource in storage.
     *
     * @param PermissionUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
  public function update(PermissionUpdateRequest $request, $id)
  {
    $permission = Permission::findOrFail($id);

    $permission->update($request->all());

    return $this->showResponse($permission, 204);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy($id)
  {
    $permission = Permission::findOrFail($id);

    $permission->delete();

    return $this->showResponse($permission, 204);
  }
}
