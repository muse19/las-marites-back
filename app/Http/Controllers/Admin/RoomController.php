<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageStore;
use App\Http\Controllers\ApiController;
use App\Http\Requests\RoomStoreRequest;
use App\Http\Requests\RoomUpdateRequest;
use App\Http\Resources\RoomResource;
use App\Models\Room;
use App\Models\RoomGallery;
use Illuminate\Support\Facades\DB;

class RoomController extends ApiController
{
    public function index()
    {
        $rooms = Room::with('type')->get();

        return $this->showResponse($rooms);

    }

    public function store(RoomStoreRequest $request)
    {
        DB::beginTransaction();

        try{

            $room = Room::create($request->except('pictures','caracteristicas'));

            $room->facilities()->sync($request->caracteristicas);

            foreach ($request->pictures as $image) {
                $imageName = ImageStore::saveImage($image);

                RoomGallery::create([
                    'habitacion_id' => $room->id,
                    'imagen' => $imageName
                ]);
            }

            DB::commit();

        }catch (\Exception $e){
            DB::rollBack();

            return $this->errorResponse($e->getMessage(), 500);
        }

        return $this->showResponse($room, 201);
    }

    public function show($roomId)
    {
        $room = Room::where('id', $roomId)
            ->with('facilities:id', 'pictures:id,habitacion_id,imagen')
            ->firstOrFail();

        return $this->showResponse(new RoomResource($room));
    }

    public function update(RoomUpdateRequest $request, $id)
    {
        DB::beginTransaction();

        $room = Room::findOrFail($id);

        try {
            $room->update($request->except('pictures', 'caracteristicas'));

            $room->facilities()->sync($request->caracteristicas);

            if($request->filled('pictures')){
                foreach ($request->pictures as $image) {
                    $imageName = ImageStore::saveImage($image);
                    RoomGallery::create([
                        'habitacion_id' => $room->id,
                        'imagen' => $imageName
                    ]);
                }
            }

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();

            return $this->errorResponse($e->getMessage(), 500);
        }

        return $this->showResponse($room, 204);
    }

    public function destroy($id)
    {
        $room = Room::findOrFail($id);

        $room->delete();

        return $this->showResponse($room, 204);
    }
}
