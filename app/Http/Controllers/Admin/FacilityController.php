<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Requests\FacilityStoreRequest;
use App\Http\Requests\FacilityUpdateRequest;
use App\Models\Facility;
use Illuminate\Http\Request;

class FacilityController extends ApiController
{
    public function index()
    {
        $facilities = Facility::all();

        return $this->showResponse($facilities);
    }

    public function store(FacilityStoreRequest $request)
    {
        $facility = Facility::create($request->only('nombre','icono'));

        return $this->showResponse($facility,201);
    }

    public function show($id)
    {
        //
    }

    public function update(FacilityUpdateRequest $request, $id)
    {
        $facility = Facility::findOrFail($id);

        $facility->update($request->only('nombre','icono'));

        return $this->showResponse($facility, 204);
    }

    public function destroy($id)
    {
        $facility = Facility::findOrFail($id);

        $facility->delete();

        return $this->showResponse($facility, 204);
    }
}
