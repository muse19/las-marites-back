<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Booking;
use Illuminate\Http\Request;

class ChangeStatusBookingController extends ApiController
{
    public function __invoke(Request $request, $bookingId){

        $booking = Booking::findOrFail($bookingId);

        $booking->update(['estado_id' => $request->status]);

        return $this->showResponse($booking, 204);
    }
}
