<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Requests\InputStoreRequest;
use App\Http\Requests\InputUpdateRequest;
use App\Models\Input;
use Illuminate\Http\Request;

class InputController extends ApiController
{

    public function index()
    {
        $inputs = Input::select('id', 'unidad_id','nombre', 'cantidad', 'cantidad_min', 'cantidad_max', 'costo', 'descripcion')
            ->with('unit:id,nombre,simbolo')
            ->get();

        return $this->showResponse($inputs);
    }


    public function store(InputStoreRequest $request)
    {
        $input = Input::create([
           'unidad_id' => $request->unidad_id,
           'nombre' => $request->nombre,
           'descripcion' => $request->descripcion,
           'cantidad' => $request->cantidad,
           'cantidad_max' => $request->cantidad_max,
           'cantidad_min' => $request->cantidad_min,
           'costo' => $request->costo
        ]);

        return $this->showResponse($input, 201);
    }


    public function show($id)
    {
        //
    }


    public function update(InputUpdateRequest $request, $inputId)
    {
        $input = Input::findOrFail($inputId);

        $input->update($request->only(
            'unidad_id',
            'nombre',
            'descripcion',
            'cantidad',
            'cantidad_max',
            'cantidad_min',
            'costo'
            )
        );

        return $this->showResponse($input, 204);
    }


    public function destroy($inputId)
    {
        $input = Input::findOrFail($inputId);

        $input->delete();

        return $this->showResponse($input, 204);
    }
}
