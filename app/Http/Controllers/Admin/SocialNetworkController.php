<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Requests\SocialNetworkStoreRequest;
use App\Http\Requests\SocialNetworkUpdateRequest;
use App\Models\SocialNetwork;

class SocialNetworkController extends ApiController
{

    public function index()
    {
        $socialNetworks = SocialNetwork::select('id', 'enlace', 'nombre', 'icono')->get();

        return $this->showResponse($socialNetworks);
    }


    public function store(SocialNetworkStoreRequest $request)
    {
        $socialNetwork = SocialNetwork::create($request->only('nombre', 'enlace', 'icono'));

        return $this->showResponse($socialNetwork, 201);
    }


    public function show($id)
    {
        //
    }


    public function update(SocialNetworkUpdateRequest $request, $socialNetworkId)
    {
        $socialNetwork = SocialNetwork::findOrFail($socialNetworkId);

        $socialNetwork->update($request->only('nombre', 'enlace', 'icono'));

        return $this->showResponse($socialNetwork, 204);
    }


    public function destroy($socialNetworkId)
    {
        $socialNetwork = SocialNetwork::findOrFail($socialNetworkId);

        $socialNetwork->delete();

        return $this->showResponse($socialNetwork, 204);
    }
}
