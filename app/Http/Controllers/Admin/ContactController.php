<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactStoreRequest;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends ApiController
{

    public function index()
    {
        $contacts = Contact::select('id', 'valor', 'tipo')->get();

        return $this->showResponse($contacts);
    }


    public function store(ContactStoreRequest $request)
    {
        $contact = Contact::create($request->only('valor', 'tipo'));

        return $this->showResponse($contact, 201);
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $contactId)
    {
        $contact = Contact::findOrFail($contactId);

        $contact->update($request->only('valor', 'tipo'));

        return $this->showResponse($contact, 204);
    }


    public function destroy($contactId)
    {
        $contact = Contact::findOrFail($contactId);

        $contact->delete();

        return $this->showResponse($contact, 204);

    }
}
