<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Requests\BookingPaymentStoreRequest;
use App\Http\Requests\BookingPaymentUpdateRequest;
use App\Models\Booking;
use App\Models\Payment;
use Illuminate\Http\Request;

class BookingPaymentController extends ApiController
{

    public function index($bookingId)
    {
        $payments = Payment::whereHas('accountReceivable', function ($q) use($bookingId){
                $q->where('reservacion_id', $bookingId);
             })
            ->with('paymentMethod')
            ->get();

        return $this->showResponse($payments);
    }

    public function store(BookingPaymentStoreRequest $request, $bookingId)
    {
        $booking = Booking::where('id', $bookingId)
            ->with('accountReceivable')
            ->firstOrFail();

        $payment = Payment::create([
            'cuenta_cobrar_id' => $booking->accountReceivable->id,
            'metodo_pago_id' => $request->metodo_pago_id,
            'descripcion' => $request->descripcion,
            'referencia' => $request->referencia,
            'monto' => $request->monto
        ]);

        return $this->showResponse($payment, 201);
    }

    public function show($id)
    {
        //
    }

    public function update(BookingPaymentUpdateRequest $request, $bookingId, $paymentId)
    {
        $booking = Booking::where('id', $bookingId)
            ->with('accountReceivable')
            ->firstOrFail();

        $payment = Payment::where('id', $paymentId)
            ->where('cuenta_cobrar_id', $booking->accountReceivable->id)
            ->firstOrFail();

        $payment->update($request->only('metodo_pago_id', 'descripcion', 'referencia'));

        return $this->showResponse($payment, 204);
    }

    public function destroy($bookingId, $paymentId)
    {
        $booking = Booking::where('id', $bookingId)
            ->with('accountReceivable')
            ->firstOrFail();

        $payment = Payment::where('id', $paymentId)
            ->where('cuenta_cobrar_id', $booking->accountReceivable->id)
            ->firstOrFail();

        $payment->delete();

        return $this->showResponse($payment, 204);
    }
}
