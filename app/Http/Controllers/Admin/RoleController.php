<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Http\Controllers\ApiController;
use App\Http\Requests\RoleStoreRequest;
use App\Http\Requests\RoleUpdateRequest;

class RoleController extends ApiController
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function index()
  {
    $roles = Role::with('permissions')->get();

    return $this->showResponse($roles);
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
  public function store(RoleStoreRequest $request)
  {
    $role = Role::create($request->except('permissions'));

    $role->permissions()->sync($request->permissions);

    return $this->showResponse($role, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleUpdateRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
  public function update(RoleUpdateRequest $request, $id)
  {
    $role = Role::findOrFail($id);

    $role->update($request->except('permissions'));

    $role->permissions()->sync($request->permissions);

    return $this->showResponse($role, 204);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy($id)
  {
    $role = Role::findOrFail($id);

    $role->delete();

    return $this->showResponse($role, 204);
  }
}
