<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;

class UserController extends ApiController
{

    public function index()
    {
        $users = User::whereHas('role', function ($q){
          return $q->whereIn('nombre', ['Administrador', 'Recepcionista', 'Cocinero']);
        })
        ->with('role:id,nombre')
        ->get();

        return $this->showResponse($users);
    }

    public function store(UserStoreRequest $request)
    {
        $user = User::create($request->only('rol_id', 'nombre', 'apellido', 'cedula', ));

        return $this->showResponse($user, 201);
    }


    public function show($id)
    {
        $user = User::findOrFail($id);
        return $this->showResponse($user);
    }

    public function update(UserUpdateRequest $request, $userId)
    {
        $user = User::findOrFail($userId);

        $user->update($request->only('rol_id', 'nombre', 'apellido', 'cedula'));

        return $this->showResponse($user, 204);
    }

    public function destroy($userId)
    {
        $user = User::findOrFail($userId);

        $user->delete();

        return $this->showResponse($user, 204);
    }
}
