<?php

namespace App\Http\Controllers\Admin;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;


class TrasactionController extends ApiController
{

    public function __invoke(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');
        $input = $request->input('input');
        $type = $request->input('type');
        $requestBy = $request->input('requestBy');

        if(!isset($from) || !isset($to)){
            $from = date('Y-m') . '-01';
            $to = date('Y-m') . '-31';
        }
        
        $transactions = Transaction::select('cantidad', 'tipo', 'insumo_id','usuario_id', 'created_at')
            ->with('input:id,nombre,unidad_id', 'user:id,nombre,apellido', 'input.unit:id,simbolo')
            ->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to)
            ->orderBy('created_at', 'desc');

        $transactions->when(isset($input), function ($query) use ($input) {
            return $query->where('insumo_id', $input);
        });

        $transactions->when(isset($type) && $type != 'all', function ($query) use ($type) {
            return $query->where('tipo', $type == 'in' ? '1' : '0');
        });

        $transactions->when(isset($requestBy), function ($query) use ($requestBy) {
            return $query->where('usuario_id', $requestBy);
        });


        return $this->showResponse($transactions->get());
    }

}
