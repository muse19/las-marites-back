<?php

namespace App\Http\Controllers\Admin;

use App\Models\Payment;
use App\Http\Controllers\ApiController;

class ConfirmPaymentController extends ApiController
{
    public function __invoke($paymentId)
    {
        $payment = Payment::findOrFail($paymentId);

        $payment->verificado = 1;

        $payment->save();

        return $this->showResponse($payment, 204);
    }
}
