<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Booking;


class BookingCompanionCapacityController extends ApiController
{
   public function __invoke($bookingId){

       $companionMaxCapacity = Booking::getCompanionMaxCapacity($bookingId);

       return $this->showResponse(['capacidad' => $companionMaxCapacity]);
   }
}
