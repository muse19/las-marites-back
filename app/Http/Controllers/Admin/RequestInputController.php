<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Http\Requests\RequestInputStoreRequest;
use App\Models\Input;
use App\Models\Transaction;

class RequestInputController extends ApiController
{
    public function __invoke(RequestInputStoreRequest $request)
    {
        DB::beginTransaction();

        $inputs = Input::whereIn('id', array_column( $request->insumos, 'id'))->get();

        try{
            foreach($inputs as $input){

                $quantity = Input::getQuantityInOrder($request->insumos, $input->id);
                $input->cantidad = $input->cantidad - $quantity;
                $input->save();
                
                Transaction::create([
                    'insumo_id' => $input->id,
                    'usuario_id' => $request->usuario_id,
                    'cantidad' => $quantity,
                    'tipo' => Transaction::OUT
                ]);
            }

            DB::commit();

            return $this->showResponse(['success' => true], 201);

        }catch(\Exception $e){
            DB::rollback();
            return $this->errorResponse($e->getMessage(), 500); 
        }

    }
}
