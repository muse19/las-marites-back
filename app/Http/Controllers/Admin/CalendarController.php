<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\RoomType;
use Illuminate\Http\Request;

class CalendarController extends ApiController
{
    public function __invoke(Request $request){
        $date = new \DateTime();

        if ($request->filled('month')) {
            $week = date("W", mktime(0, 0, 0, $request->month, "01", $request->year));
            $year = $request->year;
        } else if ($request->filled('week') && $request->filled('day')) {
            $week = date("W", mktime(0, 0, 0, $request->month, $request->year, $request->year));
            $year = $request->year;
        } else if ($request->filled('week')) {
            $week = $request->week;
            $year = $request->year;
        } else {
            $week = $date->format("W");
            $year = $date->format("Y");
        }

        $dates = $this->getWeek($week, $year);

        $data = RoomType::with(['rooms.bookings' => function ($q) use ($dates) {
            $q->whereBetween('reservaciones.fecha_llegada', [reset($dates), end($dates)])
                ->orWhere(function ($q) use ($dates) {
                    $q->whereBetween('reservaciones.fecha_salida', [reset($dates), end($dates)]);
                })
                ->with('customer', 'statuses');
        }])
            ->get();

        //$days = Carbon::parse($year . '-' . $month)->daysInMonth;
        $today = $date->format("Y-m-d");

        return [$data, $dates, $week, $year, $today];
    }

    protected function getWeek($week, $year) {
        $dto = new \DateTime();
        $dates = array();
        array_push($dates, $dto->setISODate($year,$week)->format('Y-m-d'));

        for ($i=0; $i < 6; $i++) {
            array_push($dates,$dto->modify('+1 days')->format('Y-m-d'));
        }
        return $dates;
    }
}
