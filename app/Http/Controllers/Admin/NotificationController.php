<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Notification;

class NotificationController extends ApiController
{
    public function __invoke()
    {
        $notifications = Notification::where('usuario_id', null)->get();

        return $this->showResponse($notifications);
    }
}
