<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ApiController;
use App\Models\Booking;
use App\Models\Status;

class CheckOutBookingController extends ApiController
{
    public function __invoke($bookingId){

        $booking = Booking::findOrFail($bookingId);

        $booking->update(['estado_id' => Status::CHECK_OUT, 'checkout' => now()]);

        return $this->showResponse($booking, 204);
    }
}
