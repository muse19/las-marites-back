<?php

namespace App\Http\Controllers\Admin;

use App\Events\NewOrder;
use App\Http\Controllers\ApiController;
use App\Http\Requests\BookingOrderStoreRequest;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Models\Booking;
use App\Models\Dish;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class BookingOrderController extends ApiController
{
    public function index($bookingId)
    {
        $booking = Booking::with('orders.dishes')
            ->findOrFail($bookingId);

        return $this->showResponse(new OrderCollection($booking->orders));
    }

    public function store(BookingOrderStoreRequest $request, $bookingId)
    {
        DB::beginTransaction();

        $dishes = [];

        $booking = Booking::findOrFail($bookingId);

        $dishesInOrder = Dish::whereIn('id', array_column($request->comestibles, 'id'))
            ->where('estado', Dish::STATUS_ACTIVE)
            ->select('id','precio')
            ->get();

        $total = Order::calculateTotal($dishesInOrder, $request->comestibles);

        try {
            foreach($dishesInOrder as $i => $dish) {

                $quantity = Dish::getQuantityInOrder($request['comestibles'], $dish->id);
                $price = $dish->precio;

                array_push($dishes, [
                    'cantidad' => $quantity,
                    'precio' => $price
                ]);
            }

            $dishes = array_combine($dishesInOrder->pluck('id')->toArray(), $dishes);

            $order = Order::create([
                'reservacion_id' => $booking->id,
                'numero' => time(),
                'monto' => $total,
                'estado' => Order::STATUS_PENDING
            ]);

            $order->dishes()->sync($dishes);

            DB::commit();

            event(new NewOrder($order));

            return $this->showResponse($order, 201);

        }catch (\Exception $e){
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }

    }

    public function show($bookingId, $orderId)
    {
        $order = Order::where('reservacion_id', $bookingId)
            ->where('id', $orderId)
            ->with('dishes:id,nombre,imagen')
            ->firstOrFail();

        return $this->showResponse(new OrderResource($order));
    }

    public function update(Request $request, $orderId)
    {

        return $this->showResponse($orderId, 204);
    }

    public function destroy($bookingId, $orderId)
    {
        $order = Order::where('id', $orderId)
            ->where('estado','!=', Order::STATUS_CANCELLED)
            ->firstOrFail();

        $order->estado = Order::STATUS_CANCELLED;
        $order->save();

        return $this->showResponse($order, 204);
    }
}
