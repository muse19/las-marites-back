<?php

namespace App\Http\Controllers\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\OrderCollection;
use App\Models\Order;

class PendingOrderController extends ApiController
{
    public function __invoke()
    {
        $orders = Order::with('dishes', 'booking.customer')
          ->where('estado', Order::STATUS_PENDING)
          ->whereDate('created_at', today())
          ->orderBy('created_at', 'desc')
          ->get();
       
        return $this->showResponse($orders);
    }
}
