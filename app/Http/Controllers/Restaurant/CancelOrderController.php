<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class CancelOrderController extends ApiController
{
    
    public function index()
    {
      $orders = Order::where('estado', Order::STATUS_CANCELLED)
        ->with('dishes', 'booking.customer')
        ->orderBy('created_at', 'desc')
        ->whereDate('created_at', today())
        ->get();
      
      return $this->showResponse($orders);
    }
  
    public function update($orderId)
    {
        $order = Order::findOrFail($orderId);

        if($order->estado === Order::STATUS_PENDING) {
            $order->update(['estado' => Order::STATUS_CANCELLED]);
        }

        return $this->showResponse($order, 204);
    }
}
