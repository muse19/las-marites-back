<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class DeliverOrderController extends ApiController
{
  
  public function index()
  {
    $orders = Order::where('estado', Order::STATUS_DELIVERED)
      ->with('dishes', 'booking.customer')
      ->whereDate('created_at', today())
      ->orderBy('created_at', 'desc')
      ->get();

    return $this->showResponse($orders);
  }
  
  public function update($orderId)
  {
    $order = Order::findOrFail($orderId);

    if($order->estado === Order::STATUS_CONFIRMED) {
        $order->update(['estado' => Order::STATUS_DELIVERED]);
    }

    return $this->showResponse($order, 204);
  }
}
