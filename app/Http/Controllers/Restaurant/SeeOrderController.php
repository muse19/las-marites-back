<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class SeeOrderController extends ApiController
{
    
    public function __construct()
    {
        //$this->middleware('auth:api');
    }
  
    public function __invoke($orderId)
    {
        $order = Order::findOrFail($orderId)
            ->update(['visto' => true]);

        $this->showResponse($order, 204);
    }
}
