<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Resolvers\PaymentPlatformResolver;
use App\Models\Booking;
use App\Models\Currency;

class PaymentController extends ApiController
{

  protected $paymentPlatformResolver;

  public function __construct(PaymentPlatformResolver $paymentPlatformResolver){
    $this->paymentPlatformResolver = $paymentPlatformResolver;
  }

  public function pay(Request $request)
  {

    $paymentPlatform = $this->paymentPlatformResolver->resolveService($request->payment_platform_id);

    $booking = Booking::where('localizador', $request->booking_id)->firstOrFail();

    $currencyRate = Currency::where('codigo', 'USD')->firstOrFail()->tasa;

    $amount = $booking->total/$currencyRate;

    $request->value = $amount;

    return $paymentPlatform->handlePayment($request);

  }

  public function approval(Request $request){

      $paymentPlatform = $this->paymentPlatformResolver
        ->resolveService(1);

      return $paymentPlatform->handleApproval($request);



  }

}
