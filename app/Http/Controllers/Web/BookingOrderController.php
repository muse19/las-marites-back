<?php

namespace App\Http\Controllers\Web;

use App\Models\Dish;
use App\Models\Order;
use App\Models\Booking;
use App\Events\NewOrder;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Http\Resources\OrderCollection;
use App\Http\Requests\BookingOrderStoreRequest;


class BookingOrderController extends ApiController
{
    public function index($bookingId)
    {
        $booking = Booking::with(['orders' => function($q){
            $q->orderBy('created_at', 'desc');
        }])
            ->where('usuario_id', auth()->user()->user->id)
            ->where('localizador', $bookingId)
            ->firstOrFail();


        return $this->showResponse(new OrderCollection($booking->orders));
    }

    public function store(BookingOrderStoreRequest $request, $bookingId)
    {
        DB::beginTransaction();

        $dishes = [];
        $booking = Booking::where('localizador', $bookingId)->first();


        $dishesInOrder = Dish::whereIn('id', array_column($request->comestibles, 'id'))
            ->where('estado', Dish::STATUS_ACTIVE)
            ->select('id','precio')
            ->get();

        $total = Order::calculateTotal($dishesInOrder, $request->comestibles);


        try {
            foreach($dishesInOrder as $i => $dish) {

                $quantity = Dish::getQuantityInOrder($request['comestibles'], $dish->id);
                $price = $dish->precio;

                array_push($dishes, [
                    'cantidad' => $quantity,
                    'precio' => $price
                ]);
            }

            $dishes = array_combine($dishesInOrder->pluck('id')->toArray(), $dishes);

            $order = Order::create([
                'reservacion_id' => $booking->id,
                'numero' => time(),
                'monto' => $total,
                'estado' => Order::STATUS_PENDING
            ]);

            $order->dishes()->sync($dishes);

            DB::commit();

            event(new NewOrder($order));

            return $this->showResponse($order, 201);

        }catch (\Exception $e){
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }

    }
}
