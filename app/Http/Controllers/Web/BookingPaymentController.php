<?php

namespace App\Http\Controllers\Web;

use App\Models\Payment;
use App\Http\Controllers\ApiController;

class BookingPaymentController extends ApiController
{
    public function __invoke($locator){

      $payments = Payment::whereHas('accountReceivable', function ($q) use($locator){
        $q->whereHas('booking', function ($q) use($locator){
          $q->where('localizador', $locator);
        });
      })
      ->with('paymentMethod')
      ->get();

      $payments = $payments->map(function($item){
          return [
              'nombre' => $item->paymentMethod->nombre,
              'referencia' => $item->referencia,
              'descripcion' => $item->descripcion,
              'monto' => $item->monto,
              'fecha' => $item->created_at,
          ];
      });

      return $this->showResponse($payments);
    }
}
