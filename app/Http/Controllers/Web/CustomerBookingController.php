<?php

namespace App\Http\Controllers\Web;

use App\Models\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Http\Resources\Web\CustomerBookingResource;
use App\Http\Resources\Web\CustomerBookingCollection;

class CustomerBookingController extends ApiController
{
    public function index(){

      $bookings = Booking::where('usuario_id', auth()->user()->user->id)
        ->withCount('rooms', 'companions')
        ->with('status')
        ->orderBy('created_at', 'desc')
        ->get();

      return $this->showResponse(new CustomerBookingCollection($bookings));
    }

    public function show($locator){
        
        $booking = Booking::where('usuario_id', auth()->user()->user->id)
          ->withCount('rooms', 'companions')
          ->where('localizador', $locator)
          ->firstOrFail();
  
        return $this->showResponse(new CustomerBookingResource($booking));
    }
}
