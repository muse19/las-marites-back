<?php

namespace App\Http\Controllers\Web;

use App\Models\Room;
use App\Models\User;
use App\Models\Status;
use App\Models\Booking;
use App\Events\NewBooking;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Web\RoomBookingStoreRequest;
use App\Models\Role;
use Error;
use Exception;
use Facade\Ignition\QueryRecorder\Query;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Database\QueryException;

class RoomBookingController extends ApiController
{
    public function __invoke(RoomBookingStoreRequest $request){

        $priceRooms = [];

        $rooms = Room::whereIn('id', $request->habitaciones)
            ->select('id', 'precio')
            ->get();

        $roomsAvailable = Room::checkAvailability($request->habitaciones, $request->fecha_llegada, $request->fecha_salida);

        if (!$roomsAvailable->count() == count($request->habitaciones)) {

            $data = [
                'message' => 'Habitaciones no disponibles.',
                'rooms' => array_values(array_diff($request->habitaciones, $roomsAvailable->pluck('id')->toArray()))
            ];

            return $this->errorResponse($data, 409);
        }

        $customerId = auth()->user()->usuario_id ?? null;

        DB::beginTransaction();

        try {

            if(!Auth::check()){

                $customerRole = Role::where('slug', 'customer')->first();

                $customer = User::create([
                    'nombre' => $request->customer['nombre'],
                    'apellido' => $request->customer['apellido'],
                    'nacionalidad' => $request->customer['nacionalidad'],
                    'cedula' => $request->customer['cedula'],
                    'codigo_pais' => $request->customer['codigo_pais'] ?? null,
                    'telefono' => $request->customer['telefono'] ?? null,
                    'pais_id' => $request->customer['pais_id'],
                    'direccion' => $request->customer['direccion'],
                    'rol_id' => $customerRole->id,
                ]);

                $customer->account()->create([
                    'correo' => $request->customer['correo'],
                    'contrasenia' => bcrypt($request->customer['contrasenia']),
                ]);

                $customerId = $customer->id;
                
            }

            $total = Booking::calculateTotal($rooms, $request->fecha_llegada, $request->fecha_salida);

            $booking = Booking::create([
                'localizador' => Str::random(9),
                'estado_id' => Status::PENDING,
                'usuario_id' => $customerId,
                'fecha_llegada' => $request->fecha_llegada,
                'fecha_salida' => $request->fecha_salida,
                'total' => $total,
                'cantidad_personas' => $request->cantidad_personas,
                'observaciones' => $request->observaciones
            ]);

            foreach ($rooms as $room){
                array_push($priceRooms, [
                    'precio' => $room->precio,
                    'fecha_llegada' => $request->fecha_llegada,
                    'fecha_salida' => $request->fecha_salida
                ]);
            }

            $data = array_combine($rooms->pluck('id')->toArray(), $priceRooms);

            $booking->rooms()->sync($data);

            DB::commit();

            event(new NewBooking($booking));

            return $this->showResponse($booking, 201);

        }catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage(), 500);

        }
    }
}
