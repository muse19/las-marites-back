<?php

namespace App\Http\Controllers\Web;

use App\Models\Booking;
use App\Http\Controllers\ApiController;

class BookingCompanionController extends ApiController
{
    public function __invoke($locator)
    {
      $booking = Booking::where('localizador', $locator)
            ->where('usuario_id', auth()->user()->user->id)
            ->with('companions:id,nacionalidad,cedula,nombre,apellido')
            ->first();

      $companions = $booking->companions->map(function($item) {
          return [
              'cedula' => $item->cedula,
              'nombre' => $item->nombre,
              'apellido' => $item->apellido,
              'nacionalidad' => $item->nacionalidad,
          ];
      });

      return $this->showResponse($companions);
    }
}
