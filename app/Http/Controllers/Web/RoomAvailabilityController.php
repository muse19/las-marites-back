<?php

namespace App\Http\Controllers\Web;

use App\Models\Room;
use App\Models\Status;
use App\Http\Controllers\ApiController;
use App\Http\Resources\Web\RoomCollection;
use App\Http\Requests\Web\RoomAvailabilityRequest;

class RoomAvailabilityController extends ApiController
{
    public function __invoke(RoomAvailabilityRequest $request){
        $from = $request->from;
        $to = $request->to;

        $rooms = Room::whereDoesntHave('bookings', function($q) use ($from, $to){
            $q->where('estado_id', '!=', Status::CANCELED)
                ->where(function($q) use ($from, $to) {
                    $q->where('reservaciones.fecha_llegada', '<=', $from)
                        ->where('reservaciones.fecha_salida', '>', $from);
                })
                ->orWhere(function($q) use ($from, $to){
                    $q->where('reservaciones.fecha_llegada', '<', $to)
                        ->where('reservaciones.fecha_salida', '>=', $to);
                })
                ->orWhere(function($q) use ($from, $to){
                    $q->where('reservaciones.fecha_llegada', '>=', $from)
                        ->where('reservaciones.fecha_salida', '<=', $to);
                });
        })
        ->where('estado', Room::ENABLE)
        ->select('nombre', 'precio', 'tipo_habitacion_id', 'id')
        ->with('type:id,nombre,capacidad', 'pictures:id,imagen,habitacion_id', 'facilities:nombre,icono')
        ->get();

        return $this->showResponse( new RoomCollection($rooms));
    }
}
