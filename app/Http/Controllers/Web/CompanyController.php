<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\ApiController;
use App\Models\Company;
use App\Models\Contact;
use App\Models\Gallery;
use App\Models\SocialNetwork;

class CompanyController extends ApiController
{
    public function __invoke(){
        $company = Company::select('nombre', 'logo', 'imagen_principal','sobre_nosotros', 'direccion', 'latitud', 'longitud')->first();

        $socialNetworks = SocialNetwork::select('nombre', 'icono', 'enlace')->get();

        $contacts = Contact::select('valor', 'tipo')->get();

        $gallery = Gallery::select('imagen')->get();

        $data = [
          'company' => $company,
          'social_networks' => $socialNetworks,
          'contacts' => $contacts,
          'gallery' => $gallery
        ];

        return $this->showResponse($data);
    }
}
