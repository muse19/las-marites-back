<?php

namespace App\Http\Controllers\Web;

use App\Models\Booking;
use App\Http\Controllers\ApiController;
use App\Http\Resources\BookingAccountReceivableResource;

class BookingBalanceController extends ApiController
{
    public function __invoke($locator)
    {
        $balance = Booking::where('localizador', $locator)
          ->where('usuario_id', auth()->user()->user->id)
          ->with('rooms.type', 'services', 'orders', 'accountReceivable:id,reservacion_id,saldo')
          ->first();

        return $this->showResponse(new BookingAccountReceivableResource($balance));
    }
}
