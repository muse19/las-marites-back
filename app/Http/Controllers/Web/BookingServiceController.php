<?php

namespace App\Http\Controllers\Web;

use App\Models\Booking;
use App\Http\Controllers\ApiController;

class BookingServiceController extends ApiController
{
    public function __invoke($locator)
    {
      $services = Booking::where('localizador', $locator)
        ->with('services:id,nombre')
        ->where('usuario_id', auth()->user()->user->id)
        ->firstOrFail()
        ->services;

      return $this->showResponse($services);
    }
}
