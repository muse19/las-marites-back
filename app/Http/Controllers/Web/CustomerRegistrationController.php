<?php

namespace App\Http\Controllers\Web;

use App\Models\Role;
use App\Models\User;
use App\Models\Account;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Web\CustomerRegistrationRequest;

class CustomerRegistrationController extends ApiController
{
    public function __invoke(CustomerRegistrationRequest $request)
    {
        $data = $request->all();
        $data['rol_id'] = Role::where('slug', 'customer')->first()->id;

        DB::beginTransaction();

        try
        {
            $customer = User::create([
                'nombre' => $data['nombre'],
                'apellido' => $data['apellido'],
                'rol_id' => $data['rol_id'],
            ]);
    
            $account = Account::create([
                'usuario_id' => $customer->id,
                'correo' => $request->correo,
                'contrasenia' => bcrypt($request->contrasenia),
            ]);
    
            $token = auth()->login($account);   
            
            DB::commit();
    
        } catch(\Exception $e){
            DB::rollback();
            return $this->errorResponse($e->getMessage(), 409);
        }

        return $this->showResponse(['customer' => $customer, 'access_token' => $token],201);

    }
}
