<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\ApiController;
use App\Http\Resources\Web\RoomListCollection;
use App\Http\Resources\Web\RoomResource;
use App\Models\Room;
use Illuminate\Http\Request;

class RoomController extends ApiController
{
    public function index(){

        $rooms = Room::where('estado', Room::ENABLE)
            ->with('pictures:habitacion_id,imagen', 'type', 'facilities')
            ->get();

        return $this->showResponse(new RoomListCollection($rooms));
    }

    public function show($roomId){

        $room = Room::where('id', $roomId)
            ->where('estado', Room::ENABLE)
            ->with('pictures', 'facilities', 'type')
            ->firstOrFail();

        return $this->showResponse(new RoomResource($room));
    }
}
