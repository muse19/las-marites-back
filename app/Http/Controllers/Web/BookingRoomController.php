<?php

namespace App\Http\Controllers\Web;

use App\Models\Booking;
use App\Http\Controllers\ApiController;
use App\Http\Resources\Web\RoomListCollection;


class BookingRoomController extends ApiController
{
    public function __invoke($locator){

      $booking = Booking::with('rooms.type', 'rooms.pictures')
        ->where('usuario_id', auth()->user()->user->id)
        ->where('localizador', $locator)
        ->firstOrFail();

      return $this->showResponse(new RoomListCollection($booking->rooms));
    }
}
