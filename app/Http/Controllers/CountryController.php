<?php

namespace App\Http\Controllers;

class CountryController extends ApiController
{
    public function __invoke(){

        $countries = json_decode(file_get_contents(base_path('public/countries.json')), true);

        return $this->showResponse($countries);
    }
}
