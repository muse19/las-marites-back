<?php


namespace App\Http\Controllers\Auth;


use App\Models\Account;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\UserProfileResource;


class AuthController extends ApiController
{
    public function login(Request $request)
    {

        $user = Account::where('correo', $request->correo)
            ->with('user.role')
            ->first()
            ->user ?? null;

        if(!$user){
            return $this->errorResponse('Correo y/o contraseña incorrecto.', 400);
        }

        if(!$user->hasRole(['receptionist', 'admin']) && $request->path() === 'api/admin/auth/login'){
            return $this->errorResponse('Correo y/o contraseña incorrecto.', 400);
        }

        if(!$user->hasRole(['customer']) && $request->path() === 'api/auth/login'){
            return $this->errorResponse('Correo y/o contraseña incorrecto.', 400);
        }

        if (! $token = auth()->attempt(['correo' => $request->correo, 'password' => $request->contrasenia])) {
            return $this->errorResponse('Correo y/o contraseña incorrecto.', 400);
        }

        return $this->respondWithToken($token);
    }

    public function me()
    {
        return $this->showResponse(new UserProfileResource(auth()->user()->user->load('bookings')));
    }

    public function logout()
    {
        auth()->logout();

        return $this->showResponse(['message' => 'Desconectado con exito']);
    }


    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }


    protected function respondWithToken($token)
    {
        return $this->showResponse([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
