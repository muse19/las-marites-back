<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Storage;

class ImageStore
{
    public static function saveImage($image_64) {
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];

        $replace = substr($image_64, 0, strpos($image_64, ',')+1);

        $image = str_replace($replace, '', $image_64);

        $image = str_replace(' ', '+', $image);

        $imageName = time().'.'.$extension;

        Storage::put($imageName, base64_decode($image));

        return $imageName;
    }

    public static function deleteImage($imageName){

        $result = Storage::delete($imageName);

        return $result;
    }
}
