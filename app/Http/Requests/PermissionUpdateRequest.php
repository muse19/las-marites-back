<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PermissionUpdateRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'clasificacion_id' => 'required',
      'nombre' => 'required',
      'slug' => ['required', Rule::unique('permisos')->ignore($this->permission)]
    ];
  }

  public function messages()
  {
    return [
      'clasificacion_id.required' => 'El campo es obligatorio.',
      'nombre.required' => 'El campo es obligatorio.',
      'slug.required' => 'El campo es obligatorio.',
      'slug.unique' => 'El slug del permiso ya está en uso.'
    ];
  }
}
