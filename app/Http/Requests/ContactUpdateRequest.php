<?php

namespace App\Http\Requests;

use App\Models\Contact;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContactUpdateRequest extends FormRequest
{
    protected  $type;

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $this->type = $this->tipo;

        return [
            'valor' => ['required', $this->type === Contact::TYPE_EMAIL ? 'email': '', Rule::unique('contactos')->ignore($this->contact)],
            'tipo' => ['required', Rule::in([Contact::TYPE_EMAIL, Contact::TYPE_PHONE])]
        ];
    }

    public function messages()
    {
        return [
            'valor.required' => 'El campo es obligatorio.',
            'valor.email' => 'Debe ser un correo electrónico válido.',
            'valor.unique' => "El {$this->type} ya ha sido registrado.",
            'tipo.required' => 'El campo es obligatorio.',
            'tipo.in' => "Debe ser de tipo " . Contact::TYPE_EMAIL. " o " .Contact::TYPE_PHONE
        ];
    }
}
