<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestInputStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'usuario_id' => 'required',
            'insumos' => 'array|min:1',
            'insumos.*.id' => 'required',
            'insumos.*.cantidad' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'usuario_id.required' => 'El campo es obligatorio.',
            'insumos.min' => 'Debe seleccionar al menos :min elemento.',
            'insumos.*.id.required' => 'El campo es obligatorio.',
            'insumos.*.cantidad.required' => 'El campo es obligatorio.',
        ];
    }
}
