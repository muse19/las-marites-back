<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoomTypeUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => ['required', Rule::unique('tipo_habitaciones')->whereNull('deleted_at')->ignore($this->room_type)],
            'capacidad' => 'required|integer|min:1'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo es obligatorio.',
            'nombre.unique' => 'El nombre del tipo de habitación ya ha sido registrado.',
            'capacidad.required' => 'El campo es obligatorio.',
            'capacidad.integer' => 'Debe ser un número entero.',
            'capacidad.min' => 'La capacidad deber ser mímino :min.'
        ];
    }
}
