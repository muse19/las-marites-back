<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InputUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => ['required', Rule::unique('insumos')->whereNull('deleted_at')->ignore($this->input)],
            'cantidad' => 'required|integer',
            'cantidad_max' => 'required|integer',
            'cantidad_min' => 'required|integer',
            'costo' => 'required',
            'unidad_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo es obligatorio.',
            'nombre.unique' => 'El nombre del insumo ya ha sido registrado.',
            'cantidad.required' => 'El campo es obligatorio.',
            'cantidad_max.required' => 'El campo es obligatorio.',
            'cantidad_min.required' => 'El campo es obligatorio.',
            'costo.required' => 'El campo es obligatorio.',
            'unidad_id.required' => 'El campo es obligatorio.'
        ];
    }
}
