<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingCompanionStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required|max:60',
            'apellido' => 'required|max:60',
            'cedula' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo es obligatorio.',
            'apellido.required' => 'El campo es obligatorio.',
            'cedula.required' => 'El campo es obligatorio.',
        ];
    }
}
