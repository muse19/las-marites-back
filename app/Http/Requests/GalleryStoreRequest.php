<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GalleryStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'pictures' => 'required|min:1|array'
        ];
    }

    public function messages()
    {
        return [
            'pictures.required' => 'El campo es obligatorio.',
            'pictures.min' => 'Debe tener al menos :min elemento.'
        ];
    }
}
