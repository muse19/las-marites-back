<?php

namespace App\Http\Requests;

use App\Models\Contact;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContactStoreRequest extends FormRequest
{

    protected  $type;

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $this->type = $this->tipo;

        return [
            'valor' => ['required', $this->type === Contact::TYPE_EMAIL ? 'email': 'regex:/^[0-9]{11}$/', Rule::unique('contactos') ],
            'tipo' => ['required', Rule::in([Contact::TYPE_EMAIL, Contact::TYPE_PHONE])]
        ];
    }

    public function messages()
    {
        return [
            'valor.required' => 'El campo es obligatorio.',
            'valor.email' => 'Debe ser un correo electrónico válido.',
            'valor.unique' => "El {$this->type} ya ha sido registrado.",
            'tipo.required' => 'El campo es obligatorio.',
            'tipo.in' => "Debe ser de tipo " . Contact::TYPE_EMAIL. " o " .Contact::TYPE_PHONE,
            'valor.regex' => 'Debe ser un número telefónico válido. Solo números y 11 dígitos de longitud.',
        ];
    }
}
