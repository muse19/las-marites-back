<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FacilityUpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => ['required', Rule::unique('caracteristicas')->ignore($this->facility)],
            'icono' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo es obligatorio.',
            'nombre.unique' => 'El nombre de la característica ya ha sido registrado.',
            'icono.required' => 'El campo es obligatorio.'
        ];
    }
}
