<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseOrderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'proveedor_id' => 'required',
            'insumos' => 'required|array|min:1',
            'insumos.*.id' => 'required',
            'insumos.*.cantidad' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'proveedor_id.required' => 'El campo es obligatorio.',
            'insumos.required' => 'El campo es obligatorio.',
            'insumos.*.id.required' => 'El id del insumo es obligatorio.',
            'insumos.*.cantidad.required' => 'La catidad del insumo es obligatoria.',
            'insumos.*.cantidad.integer' => 'La catidad del insumo debe ser un entero.'
        ];
    }
}
