<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'habitaciones' => 'required',
            'fecha_llegada' => 'required|date',
            'fecha_salida' => 'after:fecha_llegada|date',
            'usuario_id' => 'required',
            'estado_id' => 'required',
            'cantidad_personas' => 'required|integer|min:1',
        ];
    }

    public function messages()
    {
        return [
            'habitaciones.required' => 'El campo es obligatorio.',
            'fecha_llegada.required' => 'El campo es oblogatorio.',
            'fecha_llegada.date' => 'Debe ser un formato de fecha válido.',
            'fecha_salida.required' => 'El campo es obligatorio.',
            'fecha_salida.after' => 'La fecha de salida no puede ser menor a la fecha de llegada.',
            'fecha_salida.date' => 'Debe ser un formato de fecha válido.',
            'usuario_id.required' => 'El campo es obligatorio.',
            'estado_id.required' => 'El campo es obligatorio.',
            'cantidad_personas.required' => 'El campo es obligatorio.',
            'cantidad_personas.integer' => 'Debe ser un número entero.',
            'cantidad_personas.min' => 'Debe ser mayor a 0.',
        ];
    }
}
