<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BookingOrderStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'comestibles' => 'array|min:1|required',
            'comestibles.*.id' => ['required', Rule::exists('comestibles')->whereNull('deleted_at')],
            'comestibles.*.cantidad' => 'integer|min:1'
        ];
    }

    public function messages()
    {
        return [
            'comestibles.array' => 'Debe ser un array.',
            'comestibles.min' => 'Debe tener al menos :min elemento.',
            'comestibles.required' => 'El campo es obligatorio.',
            'comestibles.*.id.required' => 'El campo es obligatorio.',
            'comestibles.*.id.exists' => 'El comestible no se encuentra registrado.',
            'comestibles.*.cantidad.integer' => 'Debe ser un entero.',
            'comestibles.*.cantidad.min' => 'La cantidad debe ser mínimo :min',
        ];
    }
}
