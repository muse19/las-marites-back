<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingServiceStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'servicio_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'servicio_id.required' => 'El campo es obligatorio.',
        ];
    }
}
