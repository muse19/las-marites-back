<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RoomBookingStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        
        $rules = [
            'habitaciones' => 'required',
            'fecha_llegada' => 'required|date',
            'fecha_salida' => 'after:fecha_llegada|date',
            'cantidad_personas' => 'required|integer|min:1',
        ];

        if (!Auth::check()) $rules + [
            'customer.nombre' => 'required',
            'customer.apellido' => 'required',
            'customer.nacionalidad' => 'required',
            'customer.cedula' => 'required|unique:usuarios,cedula',
            'customer.correo' => 'required|email|unique:cuenta,correo',
            'customer.pais_id' => 'required',
            'customer.direccion' => 'required',
            'customer.contrasenia' => 'required',
            'customer.contrasenia_confirmation' => 'required|same:customer.contrasenia',
        ];
        
        return $rules;
    }

    public function messages()
    {
        return [
            'habitaciones.required' => 'El campo es obligatorio.',
            'fecha_llegada.required' => 'El campo es oblogatorio.',
            'fecha_llegada.date' => 'Debe ser un formato de fecha válido.',
            'fecha_salida.required' => 'El campo es obligatorio.',
            'fecha_salida.after' => 'La fecha de salida no puede ser menor a la fecha de llegada.',
            'fecha_salida.date' => 'Debe ser un formato de fecha válido.',
            'customer.nombre.required' => 'El campo es obligatorio.',
            'customer.apellido.required' => 'El campo es obligatorio.',
            'customer.nacionalidad.required' => 'El campo es obligatorio.',
            'customer.cedula.required' => 'El campo es obligatorio.',
            'customer.cedula.unique' => 'La cedula de identidad ya ha sido registrada.',
            'customer.correo.required' => 'El campo es obligatorio.',
            'customer.correo.email' => 'Debe ser un correo electrónico valido.',
            'customer.correo.unique' => 'El correo electrónico ya ha sido registrado.',
            'customer.pais_id.required' => 'El campo es obligatorio.',
            'customer.direccion.required' => 'El campo es obligatorio.',
            'customer.contrasenia.required' => 'El campo es obligatorio.',
            'customer.contrasenia_confirmation.required' => 'El campo es obligatorio.',
            'customer.contrasenia_confirmation.same' => 'Las contraseñas no coinciden.',
            'cantidad_personas.required' => 'El campo es obligatorio.',
            'cantidad_personas.integer' => 'Debe ser un número entero.',
            'cantidad_personas.min' => 'Debe ser mayor a 0.',
        ];
    }
}
