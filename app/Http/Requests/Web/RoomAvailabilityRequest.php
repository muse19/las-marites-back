<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;

class RoomAvailabilityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'required|after_or_equal:today',
            'to' => 'required|after:from',
        ];
    }

    public function messages()
    {
        return [
            'from.required' => 'Debe ingresar una fecha de inicio',
            'from.after_or_equal' => 'La fecha de inicio debe ser mayor o igual a la fecha actual',
            'to.required' => 'Debe ingresar una fecha de fin',
            'to.after' => 'La fecha de fin debe ser posterior a la fecha de inicio',
        ];
    }
}
