<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'apellido' => 'required',
            'correo' => 'required|email|unique:cuenta,correo',
            'contrasenia' => 'required|min:6',
            'contrasenia_confirmation' => 'required|same:contrasenia'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo es obligatorio.',
            'apellido.required' => 'El campo es obligatorio.',
            'correo.required' => 'El campo es obligatorio.',
            'correo.email' => 'Debe ser un correo electrónico valido.',
            'correo.unique' => 'El correo electrónico ya está en uso.',
            'contrasenia.required' => 'El campo es obligatorio.',
            'contrasenia.min' => 'La contraseña debe tener al menos 6 caracteres.',
            'contrasenia_confirmation.required' => 'El campo es obligatorio.',
            'contrasenia_confirmation.same' => 'Las contraseñas no coinciden.'
        ];
    }
}
