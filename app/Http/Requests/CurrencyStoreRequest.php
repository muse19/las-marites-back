<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurrencyStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'codigo' => 'required|max:3|unique:divisas',
            'nombre' => 'required|max:50',
            'tasa' => 'required|numeric|min:0.01',
        ];
    }

    public function messages()
    {
       return [
        'codigo.required' => 'El campo es obligatorio.',
        'codigo.max' => 'El código no puede tener más de 3 caracteres.',
        'codigo.unique' => 'El código ya ha sido registrado.',
        'nombre.required' => 'El campo es obligatorio.',
        'nombre.max' => 'El nombre no puede tener más de 50 caracteres.',
        'tasa.required' => 'El campo es obligatorio.',
        'tasa.numeric' => 'El valor debe ser numérico.',
        'tasa.min' => 'El valor debe ser mayor a :min.',

       ];
    }


}
