<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DishStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'categoria_id' => ['required', Rule::exists('categorias', 'id')->whereNull('deleted_at')],
            'nombre' => ['required', Rule::unique('comestibles')->whereNull('deleted_at')],
            'descripcion' => 'required',
            'imagen' => 'required',
            'precio' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'categoria_id.required' => 'El campo es obligatorio.',
            'categoria_id.exists' => 'La categoria no se encuentra registrada.',
            'nombre.required' => 'El campo es obligatorio.',
            'nombre.unique' => 'El nombre del plato ya ha sido registrado.',
            'imagen.required' => 'El campo es obligatorio.',
            'descripcion.required' => 'El campo es obligatorio.',
            'precio.required' => 'El campo es obligatorio.'
        ];
    }
}
