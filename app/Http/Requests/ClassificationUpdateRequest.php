<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ClassificationUpdateRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'nombre' => ['required', Rule::unique('clasificaciones')->ignore($this->classification)->whereNull('deleted_at')]
    ];
  }

  public function messages()
  {
    return [
      'nombre.required' => 'El campo es obligatorio.',
      'nombre.unique' => 'El nombre de la clasificación ya está en uso.'
    ];
  }
}
