<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassificationStoreRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'nombre' => 'required|unique:clasificaciones,nombre,NULL,id,deleted_at,NULL'
    ];
  }

  public function messages()
  {
    return [
      'nombre.required' => 'El campo es obligatorio.',
      'nombre.unique' => 'El nombre de la clasificación ya está en uso.'
    ];
  }

}
