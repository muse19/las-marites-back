<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RoleUpdateRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'slug' => ['required', Rule::unique('roles')->ignore($this->role)->whereNull('deleted_at')],
      'nombre' => 'required'
    ];
  }

  public function messages()
  {
    return [
      'slug.required' => 'El campo es obligatorio.',
      'slug.unique' => 'El nombre del rol ya está en uso.',
      'nombre.required' => 'El campo es obligatorio.',
    ];
  }
}
