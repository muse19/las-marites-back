<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ServiceUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => ['required', Rule::unique('servicios')->whereNull('deleted_at')->ignore($this->service)],
            'precio' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'nombre.required' => 'El campo es obligatorio.',
            'nombre.unique' => 'El nombre del servicio ya ha sido registrado.',
            'precio.required' => 'El campo es obligatorio.'
        ];
    }
}
