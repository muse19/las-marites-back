<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:60',
            'apellido' => 'required|max:60',
            'cedula' => ['required', Rule::unique('usuarios')->ignore($this->customer)]
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo es obligatorio.',
            'apellido.required' => 'El campo es obligatorio.',
            'cedula.required' => 'El campo es obligatorio.',
            'cedula.unique' => 'La cedula de identidad ya se encuentra registrada.'
        ];
    }
}
