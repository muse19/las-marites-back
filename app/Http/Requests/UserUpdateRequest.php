<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
          'nombre' => 'required|max:60',
          'apellido' => 'required|max:60',
          'cedula' => ['required', Rule::unique('usuarios')->ignore($this->customer)],
          'rol_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'rol_id' => 'El campo es obligatorio.',
            'nombre.required' => 'El campo es obligatorio.',
            'apellido.required' => 'El campo es obligatorio.',
            'cedula.required' => 'El campo es obligatorio.',
            'cedula.unique' => 'La cedula de identidad ya se encuentra registrada.'
        ];
    }
}
