<?php

namespace App\Http\Requests;

use App\Models\SupplierInvoice;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SupplierInvoiceStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'proveedor_id' => ['required', Rule::exists('proveedores', 'id')->whereNull('deleted_at'),],
            'numero' => ['required', Rule::unique('facturas_proveedor')->whereNull('deleted_at')],
            'insumos' => [Rule::requiredIf($this->tipo === SupplierInvoice::TYPE_PRODUCT)],
            'insumos.*.id' => 'required',
            'insumos.*.cantidad' => 'required|min:1|integer',
            'insumos.*.costo' => 'required|min:1|numeric',
            'tipo' => ['required', Rule::in([SupplierInvoice::TYPE_SERVICE, SupplierInvoice::TYPE_PRODUCT])],
            'total' => [Rule::requiredIf($this->tipo === SupplierInvoice::TYPE_SERVICE)],
            'descripcion' => [Rule::requiredIf($this->tipo === SupplierInvoice::TYPE_SERVICE)]
        ];
    }

    public function messages()
    {
        return [
            'proveedor_id.required' => 'El campo es obligatorio.',
            'proveedor_id.exists' => 'El proveedor específicado no se encuentra registrado.',
            'numero' => 'El campo es obligatorio.',
            'numero.unique' => ' El número ya se encuentra registrado.',
            'insumos.required' => 'El campo es obligatorio.',
            'insumos.*.id.required' => 'El campo es obligatorio.',
            'insumos.*.cantidad.required' => 'El campo es obligatorio.',
            'insumos.*.cantidad.min' => 'La cantidad debe ser de mínimo :min.',
            'insumos.*.cantidad.integer' => 'Debe ser un entero.',
            'insumos.*.costo.required' => 'El campo es obligatorio.',
            'insumos.*.costo.min' => 'La costo debe ser de mínimo :min.',
            'insumos.*.costo.numeric' => 'Debe ser númerico.',
            'tipo.required' => 'El campo es obligatorio.',
            'total.required' => 'El campo es obligatorio.',
            'descripcion.required' => 'El campo es obligatorio.',
            'tipo.in' => 'El tipo seleccionado es inválido.'

        ];
    }
}
