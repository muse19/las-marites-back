<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BookingPaymentUpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'metodo_pago_id' => ['required', Rule::exists('metodos_pago', 'id')]
        ];
    }

    public function messages()
    {
        return [
            'monto.required' => 'El campo es obligatorio.',
            'monto.numeric' => 'Debe ser un monto válido.',
            'monto.min' => 'El monto mímino permitido es :min.',
            'metodo_pago_id.required' => 'El campo es obligatorio.',
            'metodo_pago_id.exists' => 'El método de pago no se encuentra registrado.'
        ];
    }
}
