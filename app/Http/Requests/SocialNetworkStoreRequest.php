<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SocialNetworkStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
            'enlace' => 'required|url',
            'icono' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo es obligatorio.',
            'enlace.required' => 'El campo es obligatorio.',
            'enlace.url' => 'Debe ser un enlace válido.',
            'icono.required' => 'El campo es obligatorio.'
        ];
    }
}
