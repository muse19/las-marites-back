<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoomStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numero' => 'required|unique:habitaciones',
            'nombre' => 'required',
            'precio' => 'required|numeric',
            'caracteristicas' => 'required|min:1',
            'tipo_habitacion_id' => 'required|exists:tipo_habitaciones,id',
            //'pictures' => 'required|min:1'
        ];
    }

    public function messages()
    {
        return [
            'numero.required' => 'El campo es obligatorio.',
            'numero.unique' => 'El número de habitación ya ha sido registrado.',
            'nombre.required' => ' El campo es obligatorio.',
            'precio.required' => 'El campo es obligatorio.',
            'precio.numeric' => 'Debe ser un monto válido.',
            'caracteristicas.min' => 'Debe seleccionar al menos :min caracteristicas.',
            'tipo_habitacion_id.required' => 'El campo es obligatorio.',
            'tipo_habitacion_id.exists' => 'El tipo de habitación no se encuentra registrado.',
            'pictures.min' => 'Debe ingresar al menos :min imagen.'
        ];
    }
}
