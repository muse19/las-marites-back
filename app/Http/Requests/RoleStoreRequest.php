<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleStoreRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'slug' => 'required|unique:roles,slug,NULL,id,deleted_at,NULL',
      'nombre' => 'required'
    ];
  }

  public function messages()
  {
    return [
      'slug.required' => 'El campo es obligatorio.',
      'slug.unique' => 'El slug del rol ya está en uso.',
      'nombre' => 'El campo es obligatorio.',
    ];
  }
}
