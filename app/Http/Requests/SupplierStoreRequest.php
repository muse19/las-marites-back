<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SupplierStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rif' => ['required', Rule::unique('proveedores')->whereNull('deleted_at')],
            'razon_social' => 'required',
            'correo' => 'email'
        ];
    }

    public function messages()
    {
        return [
            'rif.required' => 'El campo es obligatorio.',
            'rif.unique' => 'El RIF ya ha sido registrado.',
            'razon_social.required' => 'El campo es obligatorio.',
            'correo.email' => 'Debe ser un formato de correo electrónico válido.'
        ];
    }
}
