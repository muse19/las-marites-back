<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'nombre' => ['required', Rule::unique('categorias')->whereNull('deleted_at')]
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo es obligatorio.',
            'nombre.unique' => 'El nombre de la categoría ya ha sido registrada.'
        ];
    }
}
