<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ServiceStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'nombre' => ['required', Rule::unique('servicios')->whereNull('deleted_at')],
            'precio' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo es obligatorio.',
            'nombre.unique' => 'El nombre del servicio ya ha sido registrado.',
            'precio.required' => 'El campo es obligatorio.'
        ];
    }
}
